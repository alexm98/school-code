public class FinancialBook extends Visitor{
	private int numberofsections = 0;
	private int numberofimages = 0;
	private int numberofproxyimages = 0;
	private int numberoftexts = 0;
	private int numberoftables = 0;

	@Override
	public void visit(Paragraph txt) {
		this.numberoftexts += 5;
	}

	@Override
	public void visit(Table table) {
		this.numberoftables += 10;
	}

	@Override
	public void visit(ProxyImage pimg) {
		this.numberofproxyimages++;
	}

	@Override
	public void visit(Image img) {
		this.numberofimages += 15;
	}

	@Override
	public void visit(Book b) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void visit(Section s) {
		this.numberofsections++;
	}

	public String toString(){
		return "Paragraphs: " + this.numberoftexts + "$\nTables: "  + this.numberoftables + "$\nImages: " + this.numberofimages + "$";
	}

	@Override
	public void visit(SpecialArtwork b) {
		// TODO Auto-generated method stub
		
	}
}
