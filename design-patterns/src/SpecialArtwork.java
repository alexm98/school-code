import java.util.List;


public class SpecialArtwork extends Element{
	private String text;

	public SpecialArtwork(String t){
		this.text = t;
	}
	
	@Override
	public void addElement(Element e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeElement(Element e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Element getChild(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	@Override
	public String toString() {
		return "****\n" + this.text + "\n****";
	}

	@Override
	public List<Element> getChildren() {
		throw new UnsupportedOperationException();
	}

}
