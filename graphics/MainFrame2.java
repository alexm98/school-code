import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
 
import javax.swing.JFrame;
import java.util.Random;
 
import com.jogamp.opengl.util.Animator;

// tema 2 si 3
 
public class MainFrame2 extends JFrame implements GLEventListener {
        private GLCanvas canvas;
        private Animator animator;
        private double xAx=0;
    	private static final double STEP_SIZE = 0.0010;
    	private double increment = STEP_SIZE;
    	private int aHouse;
 
        // For specifying the positions of the clipping planes (increase/decrease the distance) modify this variable.
        // It is used by the glOrtho method.
        private double v_size = 1.0;
 
        // Default constructor
        public MainFrame2() {
			super("Java OpenGL");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
			this.setSize(800, 600);
			this.initializeJogl();
			this.setVisible(true);
        }
 
        private void initializeJogl() {
			// Creating a new GL profile.
			GLProfile glprofile = GLProfile.getDefault();
			// Creating an object to manipulate OpenGL parameters.
			GLCapabilities capabilities = new GLCapabilities(glprofile);

			// Setting some OpenGL parameters.
			capabilities.setHardwareAccelerated(true);
			capabilities.setDoubleBuffered(true);

			// Try to enable 2x anti aliasing. It should be supported on most hardware.
			capabilities.setNumSamples(2);
			capabilities.setSampleBuffers(true);

			// Creating an OpenGL display widget -- canvas.
			this.canvas = new GLCanvas(capabilities);

			// Adding the canvas in the center of the frame.
			this.getContentPane().add(this.canvas);

			// Adding an OpenGL event listener to the canvas.
			this.canvas.addGLEventListener(this);

			// Creating an animator that will redraw the scene 40 times per second.
			this.animator = new Animator(this.canvas);

			// Starting the animator.
			this.animator.start();
        }
 
        public void init(GLAutoDrawable canvas) {
			// Obtaining the GL instance associated with the canvas.
			GL2 gl = canvas.getGL().getGL2();

			// Setting the clear color -- the color which will be used to erase the canvas.
			gl.glClearColor(0, 0, 0, 0);

			// Selecting the modelview matrix.
			gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
			

			aHouse = gl.glGenLists(1);
			gl.glNewList(aHouse, GL2.GL_COMPILE);
				drawHouse(gl);
			gl.glEndList();
        }
 
        public void display(GLAutoDrawable canvas)
        {
                GL2 gl = canvas.getGL().getGL2();
 
                // Erasing the canvas -- filling it with the clear color.
                gl.glClear(GL.GL_COLOR_BUFFER_BIT);
 
                // drawHouse(gl);
                // drawLineStripSquare(gl);
                // drawLineStrippleLoopSquare(gl);
                // drawLoopTriangle(gl);

                drawCircle(gl);
                gl.glCallList(aHouse);
                
                // drawChessBoard(gl);
 
                // Forcing the scene to be rendered.
                gl.glFlush();
        }
        
        public void drawLineStripSquare(GL2 gl){
            gl.glBegin(GL2.GL_LINE_STRIP);
				gl.glColor3f(1.0f, 0.0f, 0.0f);
				gl.glVertex2f(0.2f, 0.2f);
				gl.glVertex2f(0.4f, 0.2f);
				gl.glColor3f(0.0f, 1.0f, 0.0f);
				gl.glVertex2f(0.2f, 0.2f);
				gl.glVertex2f(0.2f, 0.4f);
				gl.glColor3f(0.0f, 0.0f, 1.0f);
				gl.glVertex2f(0.2f, 0.4f);
				gl.glVertex2f(0.4f, 0.4f);
				gl.glColor3f(1.0f, 1.0f, 0.0f);
				gl.glVertex2f(0.4f, 0.2f);
				gl.glVertex2f(0.4f, 0.4f);
            gl.glEnd();
        }
        
        public void drawLineStrippleLoopSquare(GL2 gl){
			gl.glLineStipple(1, (short) 4);
            gl.glEnable(GL2.GL_LINE_STIPPLE);
            gl.glEnable(GL2.GL_LINE_LOOP);
            gl.glBegin(GL2.GL_LINE_LOOP);
				gl.glColor3f(1.0f, 0.0f, 0.0f);
				gl.glVertex2f(0.2f, 0.2f);
				gl.glVertex2f(0.4f, 0.2f);
				gl.glColor3f(0.0f, 1.0f, 0.0f);
				gl.glVertex2f(0.2f, 0.2f);
				gl.glVertex2f(0.2f, 0.4f);
				gl.glColor3f(0.0f, 0.0f, 1.0f);
				gl.glVertex2f(0.2f, 0.4f);
				gl.glVertex2f(0.4f, 0.4f);
				gl.glColor3f(1.0f, 1.0f, 0.0f);
				gl.glVertex2f(0.4f, 0.2f);
				gl.glVertex2f(0.4f, 0.4f);
            gl.glEnd();
        }
        
        public void drawLoopTriangle(GL2 gl){
			gl.glBegin(GL2.GL_LINE_LOOP);
				gl.glVertex2f(0.2f, 0.2f);
				gl.glVertex2f(0.2f, 0.4f);
				gl.glVertex2f(0.4f, 0.4f);
			gl.glEnd();
        }
        
        public void drawCircle(GL2 gl){
			xAx = xAx + increment;
			
			//If square reaches left-top corner of the screen make increment positive
			if( xAx <= -1) {
				increment = STEP_SIZE;
			}
 
			//If square reaches right-bottom corner of the screen make increment negative
			if(xAx >= 1.4) {
				increment = -STEP_SIZE;}
			
			gl.glBegin(GL2.GL_POLYGON);
			int i = 0;
			double x, y;

			int segments = 70;
			float radianincr = 6.28f / segments; // 2*pi

			gl.glColor3f(1.0f, 1.0f, 0.0f);
			for (i = 0; i < segments; i++) {
				float angleradians = radianincr * i;
				x = 0.06 * Math.cos(angleradians);
				y = 0.06 * Math.sin(angleradians);
				gl.glVertex2d(x+ xAx,y + 0.8);
			}
			gl.glEnd();
        }
        
        public void drawHouse(GL2 gl){
            gl.glBegin(GL2.GL_LINES);
				gl.glColor3f(0.0f, 1.0f, 0.0f);
				gl.glVertex2f(0.1f, 0.1f);
				gl.glVertex2f(0.3f, 0.1f);
				gl.glColor3f(1.0f, 0.0f, 0.9f);
				gl.glVertex2f(0.1f, 0.1f);
				gl.glVertex2f(0.1f, 0.3f);
				gl.glColor3f(0.9f, 0.0f, 0.0f);
				gl.glVertex2f(0.1f, 0.3f);
				gl.glVertex2f(0.3f, 0.3f);
				gl.glColor3f(1.0f, 1.0f, 0.0f);
				gl.glVertex2f(0.3f, 0.1f);
				gl.glVertex2f(0.3f, 0.3f);
				gl.glColor3f(1.0f, 0.0f, 1.0f);
				gl.glVertex2f(0.1f,0.3f);
				gl.glVertex2f(0.2f, 0.4f);
				gl.glColor3f(0.2f, 0.3f, 0.3f);
				gl.glVertex2f(0.3f,0.3f);
				gl.glVertex2f(0.2f, 0.4f);
            gl.glEnd();
        }
        
        public void drawChessBoard(GL2 gl){
        	float size = 0.1f;
        	
    		gl.glEnable(GL.GL_CULL_FACE);
    		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);

        	for (int i = 0; i < 8; ++i) {
        	    for (int j = 0; j < 8; ++j) {
        	        if ((i + j) % 2 == 0){
        	            gl.glColor3f(1.0f, 0.0f, 0.0f); 
        	    		gl.glCullFace(GL.GL_FRONT);
        	        }
        	        else{
        	            gl.glColor3f(1.0f, 1.0f, 1.0f);
        	    		gl.glCullFace(GL.GL_BACK);
        	        }

					gl.glBegin(GL2.GL_POLYGON);
						gl.glVertex2f((float) i*size, (float) j*size);
						gl.glVertex2f((float) (i+1)*size, (float) j*size);
						gl.glVertex2f((float) (i+1)*size, (float) (j+1)*size);
						gl.glVertex2f((float) i*size, (float) (j+1)*size);
					gl.glEnd();
        	    }
        	}
        }
 
        public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height)
        {
                GL2 gl = canvas.getGL().getGL2();
 
                // Selecting the viewport -- the display area -- to be the entire widget.
                gl.glViewport(0, 0, width, height);
 
                // Determining the width to height ratio of the widget.
                double ratio = (double) width / (double) height;
 
                // Selecting the projection matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
 
                gl.glLoadIdentity();
 
                // Selecting the view volume to be x from 0 to 1, y from 0 to 1, z from -1 to 1.
                // But we are careful to keep the aspect ratio and enlarging the width or the height.
                if (ratio < 1)
                        gl.glOrtho(-v_size, v_size, -v_size, v_size / ratio, -1, 1);
                else
                        gl.glOrtho(-v_size, v_size * ratio, -v_size, v_size, -1, 1);
 
                // Selecting the modelview matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);             
        }
 
        public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged)
        {
                return;
        }
 
        public void dispose(GLAutoDrawable arg0) {
                // TODO Auto-generated method stub
        }
}