import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

import javax.swing.JFrame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

 
public class Proiect extends JFrame implements GLEventListener, KeyListener, MouseListener, MouseMotionListener{
    private GLCanvas canvas;
    private Animator animator;
 
    // GLU object used for mipmapping.
    private GLU glu;
    float angle = 0.0f;
    private TextureHandler sunTexture, earthTexture, moonTexture, cloudsTexture;
	        
    // The x position
    private float xpos;
    // The rotation value on the y axis
    private float yrot;
    // The z position
    private float zpos;
    private float heading;
    // Walkbias for head bobbing effect
    private float walkbias = 0.0f;
    // The angle used in calculating walkbias */
    private float walkbiasangle = 0.0f;
    // The value used for looking up or down pgup or pgdown
    private float lookupdown = 0.0f;
    // Define an array to keep track of the key that was pressed
    private boolean[] keys = new boolean[250];
    float dayofyear = 0f;
    
  	public Proiect(){
   		super("Java OpenGL");
   		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
   		this.setSize(800, 600);
   		this.initializeJogl();
   		this.setVisible(true);
    }
 
    private void initializeJogl(){
    	this.glu = new GLU();
    	// Creating a new GL profile.
    	GLProfile glprofile = GLProfile.getDefault();
    	// Creating an object to manipulate OpenGL parameters.
    	GLCapabilities capabilities = new GLCapabilities(glprofile);
        	
    	// Setting some OpenGL parameters.
    	capabilities.setHardwareAccelerated(true);
    	capabilities.setDoubleBuffered(true);
        	
    	// Try to enable 2x anti aliasing. It should be supported on most hardware.
    	capabilities.setNumSamples(2);
    	capabilities.setSampleBuffers(true);
    	
    	// Creating an OpenGL display widget -- canvas.
    	this.canvas = new GLCanvas(capabilities);
    	
    	this.canvas.addKeyListener(this);
    	this.canvas.addMouseListener( this);
    	this.canvas.addMouseMotionListener(this);
	
    	// Adding the canvas in the center of the frame.
    	this.getContentPane().add(this.canvas);
                
    	// Adding an OpenGL event listener to the canvas.
    	this.canvas.addGLEventListener(this);
                
    	// Creating an animator that will redraw the scene 40 times per second.
    	this.animator = new Animator(this.canvas);
    	
    	// Starting the animator.
    	this.animator.start();
    }
 
    public void init(GLAutoDrawable canvas){
    	// Obtaining the GL instance associated with the canvas.
    	GL2 gl = canvas.getGL().getGL2();
 
    	// Setting the clear color -- the color which will be used to erase the canvas.
    	gl.glClearColor(0, 0, 0, 0);
    	
    	// Selecting the modelview matrix.
    	gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
    	
    	// Create a new GLU object.
    	glu = GLU.createGLU();
        
    	//sunTexture = new TextureHandler(gl, glu, "sun.png", true);
    	sunTexture = new TextureHandler(gl, glu, "sun.jpg", true);
    	earthTexture = new TextureHandler(gl, glu, "earth.jpg", true);
    	//cloudsTexture = new TextureHandler(gl, glu, "clouds.png", true);
    	moonTexture = new TextureHandler(gl, glu, "moon.jpg", true);
    	
    	gl.glShadeModel(GL2.GL_SMOOTH);
                
    	// Activate the depth test and set the depth function.
    	gl.glEnable(GL.GL_DEPTH_TEST);
    	gl.glDepthFunc(GL.GL_LESS);
                
    	// Set The Texture Generation Mode For S To Sphere Mapping (NEW)
    	gl.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);                
    	// Set The Texture Generation Mode For T To Sphere Mapping (NEW) 
    	gl.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);
    	
    	gl.glEnable(GL2.GL_LIGHTING);
    	gl.glEnable(GL2.GL_LIGHT0);
    	gl.glEnable(GL2.GL_LIGHT1);
                
    	gl.glEnable(GL2.GL_COLOR_MATERIAL);
    	gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);
    }
        
    public void display(GLAutoDrawable canvas){
    	GL2 gl = canvas.getGL().getGL2();
    	
    	gl.glClear(GL.GL_COLOR_BUFFER_BIT);
    	gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
    	
    	
    	// Start with a fresh transformation i.e. the 4x4 identity matrix.
    	gl.glLoadIdentity();
    	
    	glu.gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    	
    	
    	gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, new float [] {0.2f, 0.0f, 0.0f, 0.1f}, 0);
    	gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float [] {0.2f, 0.2f, 0.2f, 0.1f}, 0);
    	gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float [] {-1, 0, 0, 0.1f}, 0);

    	
    	// The vector arguments represent the x, y, z, w values of the position.
    	gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float [] {-1, 0, 0, 0.1f}, 0);
 
    	// Save (push) the current matrix on the stack.
    	gl.glPushMatrix();
 
    	// this.drawCircle(gl, 0.6f, 0.0f, 0.0f);
    	// Translate the first sphere to coordinates (0,0,0).
    	gl.glTranslatef (0.0f, 0.43f, 0.0f);
    	// The vector arguments represent the R, G, B, A values.
                
    	// Compute rotation and translation angles
    	float xTrans = -xpos;
    	float yTrans = -walkbias - 0.43f;
    	float zTrans = -zpos;
    	float sceneroty = 360.0f - yrot;
    	float angleYear = ( dayofyear / 365f ) * 360f;
    	final float clocktick = 20f;
    	dayofyear = (dayofyear + (clocktick/24f)) % 365f;
         
    	// Perform operations on the scene
    	gl.glRotatef(lookupdown, 0.1f, 0.0f, 0.0f);
    	gl.glRotatef(sceneroty, 0.0f, 0.1f, 0.0f);
    	
    	gl.glTranslatef(xTrans, yTrans, zTrans);    
    	
    	gl.glMaterialfv(GL.GL_FRONT, GL2.GL_AMBIENT, new float [] {0.2f, 0.2f, 0.0f, 0.1f}, 0);
    	gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, new float [] {0.2f, 0.2f, 0.0f, 0.1f}, 0);
    	gl.glMaterialfv(GL.GL_FRONT, GL2.GL_SPECULAR, new float [] {0.2f, 0.2f, 0.0f, 0.1f}, 0);
    	gl.glMaterialfv(GL.GL_FRONT, GL2.GL_EMISSION, new float [] {0.15f, 0.15f, 0f, 0.1f}, 0);
                
    	// sun
    	this.drawSphere(gl, glu, 0.2f, true, this.sunTexture);
               
    	// Save (push) on the stack the matrix holding the transformations produced by translating the first sphere. 
    	gl.glPushMatrix();
    	
    	// earth
    	gl.glPopMatrix();
    	// gl.glRotatef (angle, 1, 0, 0);
    	gl.glRotatef(angleYear, 0.0f, 0.1f, 0.0f);
    	// Translate the second sphere to coordinates (7.5,0,0).
    	gl.glTranslatef(0.75f, 0.0f, 0.0f);
    	gl.glScalef (0.5f, 0.5f, 0.5f);
    	// Draw the second sphere.
    	this.drawSphere(gl, glu, 0.2f, true, this.earthTexture);
    	gl.glPushMatrix();
    	
    	// moon
    	gl.glPopMatrix();
        gl.glRotatef(angleYear, 0, 1, 0);
       	// Translate the third sphere to coordinates (4,0,0).
        gl.glTranslatef (0.4f, 0.0f, 0.0f);
        // Scale it to be half the size of the first one.
        gl.glScalef (0.5f, 0.5f, 0.5f);
        // Draw the third sphere.
    	this.drawSphere(gl, glu, 0.2f, true, this.moonTexture);
    	
    	// Restore (pop) from the stack the matrix holding the transformations prior to our translation of the first sphere.
    	gl.glPopMatrix();
    	
    	
    	// Check which key was pressed
    	if (keys[KeyEvent.VK_RIGHT]){
    		heading -= 2.0f;
    		yrot = heading;
    	}
    	
    	if (keys[KeyEvent.VK_LEFT]){
    		heading += 2.0f;
    		yrot = heading;
    	}
    	
    	if (keys[KeyEvent.VK_UP]){
    		zpos -= (float)Math.sin(Math.toRadians(heading)) * 0.001f; // Move On The X-Plane Based On Player Direction
    		zpos -= (float)Math.cos(Math.toRadians(heading)) * 0.001f; // Move On The Z-Plane Based On Player Direction
    		
    		if (walkbiasangle >= 359.0f)
    			walkbiasangle = 0.0f;
    		else
    			walkbiasangle += 10.0f;
    		
    		walkbias = (float)Math.sin(Math.toRadians(walkbiasangle))/20.0f; // Causes The Player To Bounce    		
    	}
    	
    	if (keys[KeyEvent.VK_DOWN]) {
    		xpos += (float)Math.sin(Math.toRadians(heading)) * 0.001f; // Move On The X-Plane Based On Player Direction
    		zpos += (float)Math.cos(Math.toRadians(heading)) * 0.001f; // Move On The Z-Plane Based On Player Direction
    		
    		if (walkbiasangle <= 1.0f)
    			walkbiasangle = 359.0f;
    		else
    			walkbiasangle -= 10.0f;
    		
    		walkbias = (float)Math.sin(Math.toRadians(walkbiasangle))/20.0f; // Causes The Player To Bounce
    	}
    	
    	if (keys[KeyEvent.VK_PAGE_UP]){
    		lookupdown += 1.0f;
    	}
    	
    	if (keys[KeyEvent.VK_PAGE_DOWN]){
    		lookupdown -= 1.0f;
    	}
    	gl.glFlush();
    	
    	// Increase the angle of rotation by 5 degrees.
    	angle += 0.5;
    }
    
    private void drawSphere(GL gl, GLU glu, float radius, boolean texturing, TextureHandler texture){
    	if (texturing){
    		texture.bind();
    		texture.enable();
    	}
    	
    	GLUquadric sphere = glu.gluNewQuadric();
    	
    	if (texturing){
    		glu.gluQuadricTexture(sphere, true);
    	}
    	glu.gluSphere(sphere, radius, 32, 32);
    	glu.gluDeleteQuadric(sphere);
    }	
    
    
    public void drawCircle(GL2 gl, float radius, float cx, float cy){
    	int segments = 70;
    	double x,y;
    	
    	gl.glBegin(GL.GL_LINE_LOOP);
    	gl.glColor3f(1.0f, 1.0f, 1.0f);
    	for(int ii = 0; ii < segments; ii++){
    		float theta = 2.0f * 3.1415926f * ii / segments;	//get the current angle

    		x = radius * Math.cos(theta);	//calculate the x component
    	    y = radius * Math.sin(theta);	//calculate the y component

    	    gl.glVertex2d(x + cx, y + cy);	//output vertex
    	}
    	gl.glEnd();
    }

    
    public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
    	GL2 gl = canvas.getGL().getGL2();
    	
    	// Selecting the viewport -- the display area -- to be the entire widget.
    	gl.glViewport(0, 0, width, height);
    	
    	// Determining the width to height ratio of the widget.
    	double ratio = (double) width / (double) height;
    	
    	// Selecting the projection matrix.
    	gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
    	
    	gl.glLoadIdentity();
    	
    	glu.gluPerspective (38, ratio, 0.1, 60.0);
    	
    	// Selecting the modelview matrix.
    	gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW); 
    }
    
    public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged) {
    	return;
    }
    
    public void dispose(GLAutoDrawable arg0) {
    	// TODO Auto-generated method stub
    }
    
    public void mouseDragged(MouseEvent arg0) {
    	// TODO Auto-generated method stub
    }
    
    public void mouseMoved(MouseEvent arg0) {
    	// TODO Auto-generated method stub
    }
			
    public void keyPressed(KeyEvent ke) {
    	if(ke.getKeyCode() < 250)
    		System.out.println("BLA"+ke.getKeyChar());
    	keys[ke.getKeyCode()] = true;
    }
		
    public void keyReleased(KeyEvent ke) {
    	System.out.println("REL"+ke.getKeyChar());
    	if (ke.getKeyCode() < 250)
    		keys[ke.getKeyCode()] = false;
    }

    public void keyTyped(KeyEvent arg0) {
    	// TODO Auto-generated method stub
    }

    public void mouseClicked(MouseEvent arg0) {
	// TODO Auto-generated method stub
			
    }

    public void mouseEntered(MouseEvent arg0) {
    	// TODO Auto-generated method stub
    }

    public void mouseExited(MouseEvent arg0) {
    	// TODO Auto-generated method stub
    }

    public void mousePressed(MouseEvent arg0) {
    	// TODO Auto-generated method stub
    }

    public void mouseReleased(MouseEvent arg0) {
    	// TODO Auto-generated method stub
    }
}
