import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;

import javax.swing.JFrame;

import com.jogamp.opengl.util.Animator;
import java.awt.BorderLayout;

public class MainFrame extends JFrame implements GLEventListener
{
	private GLCanvas canvas;
	private Animator animator;
	double equation[] = { 1f, 1f, 1f, 1f};
	
	public MainFrame()
	{
		super("Java OpenGL");
		
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setSize(800, 600);
		
		// This method will be explained later
		this.initializeJogl();
		
		this.setVisible(true);
	}

	private void initializeJogl() {
		// Obtaining a reference to the default GL profile
		GLProfile glProfile = GLProfile.getDefault();
		// Creating an object to manipulate OpenGL parameters.
		GLCapabilities capabilities = new GLCapabilities(glProfile);
		
		// Setting some OpenGL parameters.
		capabilities.setHardwareAccelerated(true);
		capabilities.setDoubleBuffered(true);
		
		// Creating an OpenGL display widget -- canvas.
		this.canvas = new GLCanvas();
		
		// Adding the canvas in the center of the frame.
		this.getContentPane().add(this.canvas);
		
		// Adding an OpenGL event listener to the canvas.
		this.canvas.addGLEventListener(this);
		
		this.animator = new Animator();
		this.animator.add(this.canvas);
		this.animator.start();
	}
	
	public void display(GLAutoDrawable canvas) {
		GL2 gl = canvas.getGL().getGL2();

		drawSquare(gl);
		
		gl.glFlush();
	}
	
	public void drawSquare(GL2 gl){
		gl.glEnable(GL2.GL_LINE);

		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(1.0f, 0.0f, 0.0f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.0f, 1.0f, 0.0f);
			gl.glVertex2f(0.2f, 0.8f);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(0.0f, 0.0f, 1.0f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.9f, 0.9f, 0.0f);
			gl.glVertex2f(0.8f, 0.4f);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(0.1f, 0.5f, 0.9f);
			gl.glVertex2f(0.2f, 0.8f);
			gl.glColor3f(0.9f, 0.2f, 0.0f);
			gl.glVertex2f(0.8f, 0.8f);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(0.0f, 0.2f, 0.2f);
			gl.glVertex2f(0.8f, 0.8f);
			gl.glColor3f(0.5f, 0.9f, 0.5f);
			gl.glVertex2f(0.8f, 0.4f);
		gl.glEnd();

		
		gl.glDisable(GL2.GL_LINE);
	}

	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}

	public void init(GLAutoDrawable canvas) {
		// Obtain the GL instance associated with the canvas.
		GL2 gl = canvas.getGL().getGL2();
		
		// Set the clear color -- the color which will be used to reset the color buffer.
		gl.glClearColor(0, 0, 0, 0);
		
		// Select the Projection matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		
		// Clear the projection matrix and set it to be the identity matrix.
		gl.glLoadIdentity();
		
		// Set the projection to be orthographic.
		// It could have been as well chosen to be perspective.
		// Select the view volume to be x in the range of 0 to 1, y from 0 to 1 and z from -1 to 1. 
		gl.glOrtho(0, 1, 0, 1, -1, 1);	
		
		// Enable additional Clip plane.
		gl.glEnable( GL2.GL_CLIP_PLANE1 );

		// Set clip plane to be defined by the 	equation arguments
		gl.glClipPlane( GL2.GL_CLIP_PLANE1 , equation, 0 );
	}

	public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
		GL2 gl = canvas.getGL().getGL2();
		
		// Select the viewport -- the display area -- to be the entire widget.
		gl.glViewport(0, 0, width, height);
		
		// Determine the width to height ratio of the widget.
		double ratio = (double) width / (double) height;
		
		// Select the Projection matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		
		gl.glLoadIdentity();
		
		// Select the view volume to be x in the range of 0 to 1, y from 0 to 1 and z from -1 to 1.
		// We are careful to keep the aspect ratio and enlarging the width or the height.
		if (ratio < 1)
			gl.glOrtho(0, 1, 0, 1 / ratio, -1, 1);
		else
			gl.glOrtho(0, 1 * ratio, 0, 1, -1, 1);

		// Return to the Modelview matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
	}
}