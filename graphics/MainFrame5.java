import java.util.ArrayList;

import javax.swing.JFrame;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;

import astro.PolarProjectionMap;
 
public class MainFrame5 extends JFrame implements GLEventListener {
        private GLCanvas canvas;
        private GLUT glut;
    	// GLU object used for mipmapping.
    	private GLU glu;
    	// Holds a reference to the PolarProjectionMap object.
    	private PolarProjectionMap ppm = null;
    	// Used to identify the display list.
    	private int ppm_list;
 
        // For specifying the positions of the clipping planes (increase/decrease the distance) modify this variable.
        // It is used by the glOrtho method.
        private double v_size = 1.0;
 
        // Default constructor
        public MainFrame5() {
			super("Java OpenGL");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
			this.setSize(800, 600);
			this.initializeJogl();
			this.setVisible(true);
        }
 
        private void initializeJogl() {
			// Creating a new GL profile.
			GLProfile glprofile = GLProfile.getDefault();
			// Creating an object to manipulate OpenGL parameters.
			GLCapabilities capabilities = new GLCapabilities(glprofile);

			// Setting some OpenGL parameters.
			capabilities.setHardwareAccelerated(true);
			capabilities.setDoubleBuffered(true);

			// Try to enable 2x anti aliasing. It should be supported on most hardware.
			capabilities.setNumSamples(2);
			capabilities.setSampleBuffers(true);

			// Creating an OpenGL display widget -- canvas.
			this.canvas = new GLCanvas(capabilities);

			// Adding the canvas in the center of the frame.
			this.getContentPane().add(this.canvas);

			// Adding an OpenGL event listener to the canvas.
			this.canvas.addGLEventListener(this);
        }
 
        public void init(GLAutoDrawable canvas) {
			// Obtaining the GL instance associated with the canvas.
			GL2 gl = canvas.getGL().getGL2();

			// Create a new GLU object.
			glu = GLU.createGLU();

			glut = new GLUT();

			this.ppm = new PolarProjectionMap(21.53, 45.17);
			// Set the separator for the line fields.
			this.ppm.setFileSep(",");
			// Read the file and compute the coordinates.
			this.ppm.initializeConstellationLines("/home/alex/workspace/lab2-gui/src/data/conlines.dat");
			this.ppm.initializeConstellationBoundaries("/home/alex/workspace/lab2-gui/src/data/cbounds.dat");
			this.ppm.initializeConstellationNames("/home/alex/workspace/lab2-gui/src/data/cnames.dat");
			this.ppm.initializeConstellationStars("/home/alex/workspace/lab2-gui/src/data/beyer.dat");
			this.ppm.initializeMessierObjects("/home/alex/workspace/lab2-gui/src/data/messier.dat");
			// Initialize here the rest of the elements from the remaining files using the corresponding methods.

			// Create the display list.
			this.ppm_list = gl.glGenLists(1);
			gl.glNewList(this.ppm_list, GL2.GL_COMPILE);
				this.makePPM(gl);
			gl.glEndList();
        }
 
        public void display(GLAutoDrawable canvas){
                GL2 gl = canvas.getGL().getGL2();
        		 
                /*
                // Specify the raster position.
        		gl.glRasterPos2d(0.5, 0.5);
        		// Render the text in the scene.
        		glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "Hello World");
        		gl.glRasterPos2d(0.2,0.2);
        		glut.glutBitmapString(GLUT.BITMAP_TIMES_ROMAN_24, "In another font");
                // Forcing the scene to be rendered.
        		*/
              
        		gl.glCallList(this.ppm_list);
        		gl.glFlush();
        }
        
        public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height) {
                GL2 gl = canvas.getGL().getGL2();
 
                // Selecting the viewport -- the display area -- to be the entire widget.
                gl.glViewport(0, 0, width, height);
 
                // Determining the width to height ratio of the widget.
                double ratio = (double) width / (double) height;
 
                // Selecting the projection matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
 
                gl.glLoadIdentity();
 
                // Selecting the view volume to be x from 0 to 1, y from 0 to 1, z from -1 to 1.
                // But we are careful to keep the aspect ratio and enlarging the width or the height.
                if (ratio < 1)
                        gl.glOrtho(-v_size, v_size, -v_size, v_size / ratio, -1, 1);
                else
                        gl.glOrtho(-v_size, v_size * ratio, -v_size, v_size, -1, 1);
 
                // Selecting the modelview matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);             
        }
        
        // We use this method for creating the display list.
    	private void makePPM(GL2 gl) {
    		final ArrayList<PolarProjectionMap.ConstellationLine> clLines = this.ppm.getConLines();
    		final ArrayList<PolarProjectionMap.ConstellationBoundaryLine> cBounds = this.ppm.getConBoundaryLines();
    		final ArrayList<PolarProjectionMap.ConstellationName> cNames = this.ppm.getConNames();
    		final ArrayList<PolarProjectionMap.ConstellationStar> cStars = this.ppm.getConStars();
    		final ArrayList<PolarProjectionMap.MessierData> cMessy = this.ppm.getMessierObjects();    		
    		
    		gl.glColor3f(0.0f, 1.0f, 0.0f);

    		gl.glBegin(GL2.GL_LINES);
    		for (PolarProjectionMap.ConstellationLine cl : clLines) {
    			if (cl.isVisible()) {
    				gl.glVertex2d(cl.getPosX1(), cl.getPosY1());
    				gl.glVertex2d(cl.getPosX2(), cl.getPosY2());
    			}
    		}
    		gl.glEnd();
    		
    		gl.glColor3f(0.0f, 0.0f, 1.0f);
    		gl.glBegin(GL2.GL_LINES);
    		for (PolarProjectionMap.ConstellationBoundaryLine cl : cBounds) {
    			if (cl.isVisible()) {
    				gl.glVertex2d(cl.getPosX1(), cl.getPosY1());
    				gl.glVertex2d(cl.getPosX2(), cl.getPosY2());
    			}
    		}
    		gl.glEnd();
    		
    		gl.glColor3f(1.0f, 1.0f, 1.0f);
			gl.glPointSize(2f);
    		gl.glBegin(GL2.GL_POINTS);
    		for (PolarProjectionMap.ConstellationStar s : cStars) {
    			if (s.isVisible()) {
    				gl.glVertex2d(s.getPosX(),s.getPosY());
    			}
    		}
			gl.glPointSize(1f);
    		gl.glEnd();
    		
    		
    		gl.glColor3f(1.0f, 1.0f, 1.0f);
    		for (PolarProjectionMap.ConstellationName cn : cNames) {
        		gl.glRasterPos2d(cn.getPosX(), cn.getPosY());
    			glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, cn.getName());
    		}    	
    		
    		gl.glColor3f(1.0f, 1.0f, 0.0f);
    		for (PolarProjectionMap.MessierData md : cMessy) {
        		gl.glRasterPos2d(md.getX(), md.getY());
    			glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, md.getName());
    		}
    	}
    	
        
        public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged)
        {
                return;
        }
 
        public void dispose(GLAutoDrawable arg0) {
                // TODO Auto-generated method stub
        }
}