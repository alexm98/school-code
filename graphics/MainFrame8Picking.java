import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.nio.IntBuffer;

import javax.swing.JFrame;

import com.jogamp.common.nio.Buffers;
import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseListener;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.jogamp.opengl.util.Animator;
 
public class MainFrame8Picking extends JFrame implements GLEventListener, KeyListener, MouseListener, MouseMotionListener{
        private GLCanvas canvas;
        private Animator animator;

        // GLU object used for mipmapping.
        private GLU glu;
       
        float angle = 0.0f;
        private TextureHandler texture1, texture2;
        
        // The x position
        private float xpos;
        // The rotation value on the y axis
        private float yrot;
         
        // The z position
        private float zpos;
         
        private float heading;
         
        // Walkbias for head bobbing effect
        private float walkbias = 0.0f;
         
        // The angle used in calculating walkbias */
        private float walkbiasangle = 0.0f;
         
        // The value used for looking up or down pgup or pgdown
        private float lookupdown = 0.0f;
         
        // Define an array to keep track of the key that was pressed
        private boolean[] keys = new boolean[250];
        
        private int mouseX, mouseY;
        // Default mode is GL_RENDER;
        private int mode = GL2.GL_RENDER;
 

        // Default constructor
        public MainFrame8Picking(){
                super("Java OpenGL");
                this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
                this.setSize(800, 600);
                this.initializeJogl();
                this.setVisible(true);
        }
 
        private void initializeJogl(){
        	this.glu = new GLU();
                // Creating a new GL profile.
                GLProfile glprofile = GLProfile.getDefault();
                // Creating an object to manipulate OpenGL parameters.
                GLCapabilities capabilities = new GLCapabilities(glprofile);
 
                // Setting some OpenGL parameters.
                capabilities.setHardwareAccelerated(true);
                capabilities.setDoubleBuffered(true);
 
                // Try to enable 2x anti aliasing. It should be supported on most hardware.
                capabilities.setNumSamples(2);
                capabilities.setSampleBuffers(true);
 
                // Creating an OpenGL display widget -- canvas.
                this.canvas = new GLCanvas(capabilities);
                
                //this.canvas.addKeyListener(this);
                //this.canvas.addMouseListener( this);
                this.canvas.addMouseMotionListener(this);
 
                // Adding the canvas in the center of the frame.
                this.getContentPane().add(this.canvas);
 
                // Adding an OpenGL event listener to the canvas.
                this.canvas.addGLEventListener(this);
 
                // Creating an animator that will redraw the scene 40 times per second.
                this.animator = new Animator(this.canvas);
 
                // Starting the animator.
                this.animator.start();
        }
 
        public void init(GLAutoDrawable canvas)
        {
                // Obtaining the GL instance associated with the canvas.
                GL2 gl = canvas.getGL().getGL2();
 
                // Setting the clear color -- the color which will be used to erase the canvas.
                gl.glClearColor(0, 0, 0, 0);
 
                // Selecting the modelview matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
                
                // Create a new GLU object.
                glu = GLU.createGLU();
 
               
                texture1 = new TextureHandler(gl, glu, "D:\\faculta\\an3\\Grafica1\\src\\texture1.jpg", true);
               //texture2 = new TextureHandler(gl, glu, "D:\\faculta\\an3\\Grafica1\\src\\texture2.jpg", true);    
                gl.glShadeModel(GL2.GL_SMOOTH);
                
                // Activate the depth test and set the depth function.
                gl.glEnable(GL.GL_DEPTH_TEST);
                gl.glDepthFunc(GL.GL_LESS);
                
             // Set The Texture Generation Mode For S To Sphere Mapping (NEW)
                gl.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);                
                // Set The Texture Generation Mode For T To Sphere Mapping (NEW) 
                gl.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);
                
                gl.glEnable(GL2.GL_LIGHTING);
                gl.glEnable(GL2.GL_LIGHT0);
                gl.glEnable(GL2.GL_LIGHT1);
                
                gl.glEnable(GL2.GL_COLOR_MATERIAL);
                gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);
 
 
        }
        
     
 
        public void display(GLAutoDrawable canvas)
        {
                GL2 gl = canvas.getGL().getGL2();
               
                
                gl.glClear(GL.GL_COLOR_BUFFER_BIT);
                gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

                
                gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, new float [] {0.2f, 0.0f, 0.0f, 0.1f}, 0);
                
                gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float [] {0.2f, 0.2f, 0.2f, 0.1f}, 0);
                // The vector arguments represent the x, y, z, w values of the position.
                gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float [] {-1, 0, 0, 0.1f}, 0);
               
                
                
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_AMBIENT, new float [] {0.2f, 0.2f, 0.0f, 0.1f}, 0);
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, new float [] {0.2f, 0.2f, 0.0f, 0.1f}, 0);
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_SPECULAR, new float [] {0.2f, 0.2f, 0.0f, 0.1f}, 0);
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_EMISSION, new float [] {0.15f, 0.15f, 0f, 0.1f}, 0);
                
                if (mode == GL2.GL_RENDER) 
                {           
                        // only clear the buffers when in GL_RENDER mode. Avoids flickering
                        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
 
                        this.drawScene(gl);
 
                }
                else
                {
                        this.pickHandle (gl, this.mouseX, this.mouseY);
                }
                
          ;
 
               
                // Check which key was pressed
                if (keys[KeyEvent.VK_RIGHT]) {
                        heading -= 2.0f;
                        yrot = heading;
                }
         
                if (keys[KeyEvent.VK_LEFT]) {
                        heading += 2.0f;
                        yrot = heading;
                }
         
                if (keys[KeyEvent.VK_UP]) {
         
                        xpos -= (float)Math.sin(Math.toRadians(heading)) * 0.001f; // Move On The X-Plane Based On Player Direction
                        zpos -= (float)Math.cos(Math.toRadians(heading)) * 0.001f; // Move On The Z-Plane Based On Player Direction
         
                        if (walkbiasangle >= 359.0f)
                                walkbiasangle = 0.0f;
                        else
                                walkbiasangle += 10.0f;
         
                        walkbias = (float)Math.sin(Math.toRadians(walkbiasangle))/20.0f; // Causes The Player To Bounce
                }
         
                if (keys[KeyEvent.VK_DOWN]) {
         
                        xpos += (float)Math.sin(Math.toRadians(heading)) * 0.001f; // Move On The X-Plane Based On Player Direction
                        zpos += (float)Math.cos(Math.toRadians(heading)) * 0.001f; // Move On The Z-Plane Based On Player Direction
         
                        if (walkbiasangle <= 1.0f)
                                walkbiasangle = 359.0f;
                        else
                                walkbiasangle -= 10.0f;
         
                        walkbias = (float)Math.sin(Math.toRadians(walkbiasangle))/20.0f; // Causes The Player To Bounce
                }
         
                if (keys[KeyEvent.VK_PAGE_UP]) {
                        lookupdown += 1.0f;
                }
         
                if (keys[KeyEvent.VK_PAGE_DOWN]) {
                        lookupdown -= 1.0f;
                }
                gl.glFlush();
                
                // Increase the angle of rotation by 5 degrees.
                angle += 0.5;
               
        }
        
        private void drawScene(GL2 gl)
        {
 
                gl.glLoadIdentity();
 
                glu.gluLookAt(0.0, 0.0, 5.0,
                        0.0, 0.0, 0.0,
                        0.0, 1.0, 0.0);
                // Remember to either add these methods or use gluLookAt(...) here.
                // aimCamera(gl, glu);
                // moveCamera();
 
 
                if (mode == GL2.GL_SELECT) 
                { 
                        // Push on the name stack the name (id) of the sphere.
                        gl.glPushName(1);
                }
                
                gl.glTranslatef (0.0f, 0.43f, 0.0f);
 
                // Then draw it.
             // Perform operations on the scene
                float xTrans = -xpos;
                float yTrans = -walkbias - 0.43f;
                float zTrans = -zpos;
                float sceneroty = 360.0f - yrot;
                gl.glRotatef(lookupdown, 0.1f, 0.0f, 0.0f);
                gl.glRotatef(sceneroty, 0.0f, 0.1f, 0.0f);
              
                gl.glTranslatef(xTrans, yTrans, zTrans);  
                this.drawSphere(gl, glu, 0.2f, true);
                if (mode == GL2.GL_SELECT) 
                { 
                        // We are done so pop the name.
                        gl.glPopName();
                }
                
                // Save (push) on the stack the matrix holding the transformations produced by translating the first sphere. 
                gl.glPushMatrix();
 
                // NOTE THE FOLLOWING ORDER OF OPERATIONS. THEY ACHIEVE A TRANSLATION FOLLOWED BY A ROTATION IN REALITY.
 
                // Rotate it with angle degrees around the X axis.
                gl.glRotatef (angle, 1, 0, 0);
                // Translate the second sphere to coordinates (10,0,0).
                gl.glTranslatef (0.4f, 0.0f, 0.0f);
                // Scale it to be half the size of the first one.
                gl.glScalef (0.5f, 0.5f, 0.5f);
                // Draw the second sphere.
                
                if (mode == GL2.GL_SELECT) 
                { 
                        // Push on the name stack the name (id) of the sphere.
                        gl.glPushName(2);
                }
                
                this.drawSphere(gl, glu, 0.2f, true);
                
             // Restore (pop) from the stack the matrix holding the transformations produced by translating the first sphere.
                gl.glPopMatrix();
 
                if (mode == GL2.GL_SELECT) 
                { 
                        // We are done so pop the name.
                        gl.glPopName();
                }
        }
        
        private void pickHandle(GL2 gl, int x, int y) {
            //Calculate the select buffer capacity and allocate data if necessary
            final int bufferSize  = 10;             
            final int capacity = Buffers.SIZEOF_INT * bufferSize;
            IntBuffer selectBuffer = Buffers.newDirectIntBuffer(capacity);

            // Send the select buffer to (J)OGL and use select mode to track object hits.
            gl.glSelectBuffer(selectBuffer.capacity(), selectBuffer);
            gl.glRenderMode(GL2.GL_SELECT);

            // Initialize the name stack.
            gl.glInitNames();

            // Get the viewport.
            int[] viewport = new int[4];
            gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
            // Get the projection matrix.
            float[] projection = new float[16];
            gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, projection, 0);

            // Switch to the projection matrix mode.
            gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
            // Save the current projection matrix.
            gl.glPushMatrix();
            // Reset the projection matrix.
            gl.glLoadIdentity();

            // Restrict region to pick object only in this region.
            // Remember to adjust the y value correctly by taking into consideration the different coordinate systems used by AWT and (J)OGL.
            glu.gluPickMatrix(x, viewport[3] - y, 1, 1, viewport, 0);

            //Load the projection matrix
            gl.glMultMatrixf(projection, 0);

            // Or redefine the perspective again.
            // glu.gluPerspective ( 38.0, (float) screenWidth / (float) screenHeight, 0.1, z_far );

            //Go back to modelview matrix for rendering.
            gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);

            // Render the scene. Note we are in GL_SELECT mode now.
            this.drawScene(gl);
        

            gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
            // Restore the saved projection matrix.  
            gl.glPopMatrix();

            // Select the modelview matrix.
            gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);

            // Return to GL_RENDER mode.
            final int hits = gl.glRenderMode(GL2.GL_RENDER);        
            mode = GL2.GL_RENDER;

            // Process the hits.
            processHits(hits, selectBuffer);
    }
        
        // Retrieved from: http://user.cs.tu-berlin.de/~schabby/PickingExample.java
        // It extracts the data in the selection buffer and writes it on the console.
        private void processHits(int hits, IntBuffer buffer) {
                int offset = 0;
                int names;
                float z1, z2;
 
                System.out.println("---------------------------------");
                System.out.println(" HITS: " + hits);
 
                for (int i = 0; i < hits; i++) {
                        System.out.println("- - - - - - - - - - - -");
                        System.out.println(" hit: " + (i + 1));
                        names = buffer.get(offset);
                        offset++;
                        z1 = (float) buffer.get(offset) / 0x7fffffff;
                        offset++;
                        z2 = (float) buffer.get(offset) / 0x7fffffff;
                        offset++;
                        System.out.println(" number of names: " + names);
                        System.out.println(" z1: " + z1);
                        System.out.println(" z2: " + z2);
                        System.out.println(" names: ");
 
                        for (int j = 0; j < names; j++) {
                                System.out.print("  " + buffer.get(offset));
                                if (j == (names - 1)) {
                                        System.out.println("<-");
                                } else {
                                        System.out.println();
                                }
                                offset++;
                        }
                        System.out.println("- - - - - - - - - - - -");
                }
                System.out.println("--------------------------------- ");
        }
 
    private void drawSphere(GL gl, GLU glu, float radius, boolean texturing){
	if (texturing){
		this.texture1.bind();
		this.texture1.enable();
	    }
 
	GLUquadric sphere = glu.gluNewQuadric();
                
	if (texturing){
		// Enabling texturing on the quadric.
		glu.gluQuadricTexture(sphere, true);
	}
	glu.gluSphere(sphere, radius, 32, 32);
	glu.gluDeleteQuadric(sphere);
    }
        public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height)
        {
 GL2 gl = canvas.getGL().getGL2();
        	 
             // Selecting the viewport -- the display area -- to be the entire widget.
             gl.glViewport(0, 0, width, height);

             // Determining the width to height ratio of the widget.
             double ratio = (double) width / (double) height;

             // Selecting the projection matrix.
             gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);

             gl.glLoadIdentity();

             glu.gluPerspective (38, ratio, 0.1, 60.0);

             // Selecting the modelview matrix.
             gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW); 
        }
 
        public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged)
        {
                return;
        }
 
        public void dispose(GLAutoDrawable arg0) {
                // TODO Auto-generated method stub
        }

		public void mouseDragged1(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

	
		public void keyPressed(KeyEvent ke) {
			
			if(ke.getKeyCode() < 250)
				System.out.println("BLA"+ke.getKeyChar());
                keys[ke.getKeyCode()] = true;
			
		}

		
		public void keyReleased(KeyEvent ke) {
	
			System.out.println("REL"+ke.getKeyChar());
	        if (ke.getKeyCode() < 250)
	        	
	                keys[ke.getKeyCode()] = false;
			
		}

		
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void mousePressed(MouseEvent me) {
			
                mouseX = me.getX();
                mouseY = me.getY();
                mode = GL2.GL_SELECT;
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		public void mouseClicked(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseDragged(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseEntered(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseMoved(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseWheelMoved(com.jogamp.newt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
}