import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

import javax.swing.JFrame;

import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.gl2.GLUT;

public class MainFrame6_2 extends JFrame implements GLEventListener{
        private GLCanvas canvas;
        private Animator animator;
        // Number of textures we want to create
        private final int NO_TEXTURES = 2;
 
        private int texture[] = new int[NO_TEXTURES];
        TextureReader.Texture[] tex = new TextureReader.Texture[NO_TEXTURES];
 
        // GLU object used for mipmapping.
        private GLU glu;
 
        // For specifying the positions of the clipping planes (increase/decrease the distance) modify this variable.
        // It is used by the glOrtho method.
        private double v_size = 1.0;
        
        private TextureHandler texture1, texture2;
 
 
        // Default constructor
        public MainFrame6_2(){
                super("Java OpenGL");
                this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
                this.setSize(800, 600);
                this.initializeJogl();
                this.setVisible(true);
        }
 
        private void initializeJogl(){
        	this.glu = new GLU();
                // Creating a new GL profile.
                GLProfile glprofile = GLProfile.getDefault();
                // Creating an object to manipulate OpenGL parameters.
                GLCapabilities capabilities = new GLCapabilities(glprofile);
 
                // Setting some OpenGL parameters.
                capabilities.setHardwareAccelerated(true);
                capabilities.setDoubleBuffered(true);
 
                // Try to enable 2x anti aliasing. It should be supported on most hardware.
                capabilities.setNumSamples(2);
                capabilities.setSampleBuffers(true);
 
                // Creating an OpenGL display widget -- canvas.
                this.canvas = new GLCanvas(capabilities);
 
                // Adding the canvas in the center of the frame.
                this.getContentPane().add(this.canvas);
 
                // Adding an OpenGL event listener to the canvas.
                this.canvas.addGLEventListener(this);
 
                // Creating an animator that will redraw the scene 40 times per second.
                this.animator = new Animator(this.canvas);
 
                // Starting the animator.
                this.animator.start();
        }
 
        public void init(GLAutoDrawable canvas)
        {
                // Obtaining the GL instance associated with the canvas.
                GL2 gl = canvas.getGL().getGL2();
 
                // Setting the clear color -- the color which will be used to erase the canvas.
                //gl.glClearColor(0, 0, 0, 0);
 
                // Selecting the modelview matrix.
                gl.glClearColor(0, 0, 0, 0);
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
                
                // Create a new GLU object.
                glu = GLU.createGLU();
                
                texture1 = new TextureHandler(gl, glu, "cat.png", true);
                texture2 = new TextureHandler(gl, glu, "cat2.png", true);    
                gl.glShadeModel(GL2.GL_SMOOTH);
                
                // Activate the depth test and set the depth function.
                gl.glEnable(GL.GL_DEPTH_TEST);
                gl.glDepthFunc(GL.GL_LESS);
                
                // Set The Texture Generation Mode For S To Sphere Mapping (NEW)
                gl.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);                
                // Set The Texture Generation Mode For T To Sphere Mapping (NEW) 
                gl.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);
                
                gl.glEnable(GL2.GL_LIGHTING);
                gl.glEnable(GL2.GL_LIGHT0);
                gl.glEnable(GL2.GL_LIGHT1);
                
                gl.glEnable(GL2.GL_COLOR_MATERIAL);
                gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);
        }
        
        public void display(GLAutoDrawable canvas){
                GL2 gl = canvas.getGL().getGL2();
 
                gl.glClear(GL.GL_COLOR_BUFFER_BIT);
                // Clear the depth buffer.
                gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
                gl.glLoadIdentity();
                
                glu.gluLookAt(0, -4.0, 1.0,
                        0.0, 0.0, 0.0,
                        0.0, 1.0, 1.5);
                
                // The vector arguments represent the R, G, B, A values.
                gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, new float [] {0.2f, 0.0f, 0.0f, 1f}, 0);
                gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float [] {0.9f, 0.9f, 0.9f, 1f}, 0);
                gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, new float [] {0.9f, 0.9f, 0.9f, 1f}, 0);
                // The vector arguments represent the x, y, z, w values of the position.
                gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float [] {-10, 0, 0, 1f}, 0);
                
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_AMBIENT, new float [] {0.8f, 0.8f, 0.0f, 1f}, 0);
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, new float [] {0.8f, 0.8f, 0.0f, 1f}, 0);
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_SPECULAR, new float [] {0.8f, 0.8f, 0.0f, 1f}, 0);
                gl.glMaterialfv(GL.GL_FRONT, GL2.GL_EMISSION, new float [] {0.5f, 0.5f, 0f, 1f}, 0);
                
                //texture2.bind();
                //texture2.enable();
                texture1.bind();
                texture1.enable();
 
                GLUquadric sun = glu.gluNewQuadric ();
                glu.gluQuadricTexture(sun, true);
                glu.gluSphere (sun, 0.2, 32, 32);
                glu.gluDeleteQuadric (sun);
                
 
                // Forcing the scene to be rendered.
                gl.glFlush();
        }
 
        public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
        	 GL2 gl = canvas.getGL().getGL2();
        	 
             // Selecting the viewport -- the display area -- to be the entire widget.
             gl.glViewport(0, 0, width, height);

             // Determining the width to height ratio of the widget.
             double ratio = (double) width / (double) height;

             // Selecting the projection matrix.
             gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);

             gl.glLoadIdentity();

             glu.gluPerspective (38, ratio, 0.1, 60.0);

             // Selecting the modelview matrix.
             gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);          
        }
 
        public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged){
                return;
        }
 
        public void dispose(GLAutoDrawable arg0){
                // TODO Auto-generated method stub
        }
}