import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

import javax.swing.JFrame;
 

public class MainFrame8	extends JFrame	implements GLEventListener{

	private GLCanvas canvas;
	private Animator animator;

	// For specifying the positions of the clipping planes (increase/decrease the distance) modify this variable.
 	// It is used by the glOrtho method.
	private double v_size = 1.0;


	// Default constructor
	public MainFrame8(){
		super("Java OpenGL");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		this.setSize(800, 600);
		this.initializeJogl();
		this.setVisible(true);
	}
	
	private void initializeJogl(){
		// Creating a new GL profile.
		GLProfile glprofile = GLProfile.getDefault();
		// Creating an object to manipulate OpenGL parameters.
		GLCapabilities capabilities = new GLCapabilities(glprofile);
		
		// Setting some OpenGL parameters.
		capabilities.setHardwareAccelerated(true);
		capabilities.setDoubleBuffered(true);

		// Try to enable 2x anti aliasing. It should be supported on most hardware.
	        capabilities.setNumSamples(2);
        	capabilities.setSampleBuffers(true);
		
		// Creating an OpenGL display widget -- canvas.
		this.canvas = new GLCanvas(capabilities);
		
		// Adding the canvas in the center of the frame.
		this.getContentPane().add(this.canvas);
		
		// Adding an OpenGL event listener to the canvas.
		this.canvas.addGLEventListener(this);
		
		// Creating an animator that will redraw the scene 40 times per second.
		this.animator = new Animator(this.canvas);
			
		// Starting the animator.
		this.animator.start();
	}
	
	public void init(GLAutoDrawable canvas){
		// Obtaining the GL instance associated with the canvas.
		GL2 gl = canvas.getGL().getGL2();
		
		// Setting the clear color -- the color which will be used to erase the canvas.
		gl.glClearColor(0, 0, 0, 0);
		
		// Selecting the modelview matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);

	}
	
	public void display(GLAutoDrawable canvas){		
		GL2 gl = canvas.getGL().getGL2();
		gl.glLoadIdentity();

		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL2.GL_ACCUM_BUFFER_BIT);

		for (int i=0; i<20; i++)
		{
			gl.glPushMatrix();
				gl.glRotated(i * 10., 0, 1, 0);
				// Draw the triangle here
				gl.glBegin(GL2.GL_LINE_LOOP);
				gl.glColor3f(1.0f, 0.0f, 0.0f);
				gl.glVertex2f(0.9f, 0.9f);
				gl.glVertex2f(0.6f, 0.8f);
				gl.glVertex2f(0.8f, 0.8f);
				gl.glEnd();
			gl.glPopMatrix();

			// Acumulate the values in the buffer
			gl.glAccum(GL2.GL_ACCUM, (float) (1.0/i /*0.25*/));

			// Next we just want to see how the images get accumulated by using the front and back buffers

			// Switch the current draw buffer to the front buffer
			gl.glDrawBuffer(GL.GL_FRONT);
			// Transfer the accumulation buffer contents into it
			gl.glAccum(GL2.GL_RETURN, (float) (1.0/(i+1)));
			// Swap the draw buffer back
			gl.glDrawBuffer(GL.GL_BACK);
		}

		gl.glAccum(GL2.GL_RETURN, (float) 1.0);

		gl.glFlush();
		

		gl.glTranslated(1, 0, 0);
		
		// Draw a colored quad here

		gl.glFlush();
	}
	
	public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
		GL2 gl = canvas.getGL().getGL2();
		
		// Selecting the viewport -- the display area -- to be the entire widget.
		gl.glViewport(0, 0, width, height);
		
		// Determining the width to height ratio of the widget.
		double ratio = (double) width / (double) height;
		
		// Selecting the projection matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		
		gl.glLoadIdentity();
		
		// Selecting the view volume to be x from 0 to 1, y from 0 to 1, z from -1 to 1.
		// But we are careful to keep the aspect ratio and enlarging the width or the height.
		if (ratio < 1)
			gl.glOrtho(-v_size, v_size, -v_size, v_size / ratio, -1, 1);
		else
			gl.glOrtho(-v_size, v_size * ratio, -v_size, v_size, -1, 1);

		// Selecting the modelview matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);		
	}
	
	public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged){
		return;
	}

	public void dispose(GLAutoDrawable arg0){
		// TODO Auto-generated method stub
	}
}