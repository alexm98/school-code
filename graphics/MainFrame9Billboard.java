import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.IntBuffer;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

import javax.swing.JFrame;

import net.java.joglutils.ThreeDS.Model3DS;
import net.java.joglutils.ThreeDS.Obj;
import net.java.joglutils.ThreeDS.Vec3;
import net.java.joglutils.ThreeDS.Face;
import net.java.joglutils.model.iModel3DRenderer;
import net.java.joglutils.model.geometry.Model;
import net.java.joglutils.test3ds.MyModel;
 
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
 
import com.jogamp.opengl.util.Animator;
import com.jogamp.common.nio.Buffers;
 
public class MainFrame9Billboard extends JFrame implements GLEventListener, KeyListener, MouseListener, MouseMotionListener{
	 private Model3DS model;
	 private TextureHandler texture3DS;
	 private GLCanvas canvas;
     private Animator animator;
     // Number of textures we want to create
     
 
     // GLU object used for mipmapping.
     private GLU glu;
       
     float angle = 0.0f;
     private TextureHandler texture1, texture2;
        
     // The x position
     private float xpos;
         
     // The rotation value on the y axis
     private float yrot;
     
     // The z position
     private float zpos;
         
     private float heading;
         
     // Walkbias for head bobbing effect
     private float walkbias = 0.0f;
        
     // The angle used in calculating walkbias */
     private float walkbiasangle = 0.0f;
     
     // The value used for looking up or down pgup or pgdown
     private float lookupdown = 0.0f;
     
     // Define an array to keep track of the key that was pressed
     private boolean[] keys = new boolean[250];
     
     private int mouseX, mouseY;
     // Default mode is GL_RENDER;
     private int mode = GL2.GL_RENDER;
        
     // The position of the camera. 
     // When using aimCamera and moveCamera you have access to those and you can use them to update the following three:     
     private double camCx;
     private double camCy;
     private double camCz;
     
     // Billboard angle, speed, direction and distance from the viewport.
     private double bbAngle = 0;
     private double bbAngleDirection = 1;
     private double bbAngleSpeed = 0.5;
     private double bbDistance = 25;
     
     private double bbCx;
     private double bbCy;
     private double bbCz;
     
     private double[] bbV1;
     private double[] bbV2;
     private double[] bbV3;
     private double[] bbV4;
     
         
     // Default constructor
     public MainFrame9Billboard(){
    	 super("Java OpenGL");
    	 this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
    	 this.setSize(800, 600);
    	 this.initializeJogl();
    	 this.setVisible(true);
     }	
 
        
     private void initializeJogl(){
    	 this.glu = new GLU();
    	 // Creating a new GL profile.
    	 GLProfile glprofile = GLProfile.getDefault();
    	 // Creating an object to manipulate OpenGL parameters.
    	 GLCapabilities capabilities = new GLCapabilities(glprofile);
    	 
        	// Setting some OpenGL parameters.
        	capabilities.setHardwareAccelerated(true);
        	capabilities.setDoubleBuffered(true);
 
        	// Try to enable 2x anti aliasing. It should be supported on most hardware.
        	capabilities.setNumSamples(2);
        	capabilities.setSampleBuffers(true);
 
        	// Creating an OpenGL display widget -- canvas.
        	this.canvas = new GLCanvas(capabilities);
                
        	this.canvas.addKeyListener(this);
        	this.canvas.addMouseListener(this);
        	this.canvas.addMouseMotionListener(this);
        	
        	// Adding the canvas in the center of the frame.
        	this.getContentPane().add(this.canvas);
 
        	// Adding an OpenGL event listener to the canvas.
        	this.canvas.addGLEventListener(this);
 
        	// Creating an animator that will redraw the scene 40 times per second.
        	this.animator = new Animator(this.canvas);
 
        	// Starting the animator.
        	this.animator.start();
        }
 
        
        public void init(GLAutoDrawable canvas){
        	// Obtaining the GL instance associated with the canvas.
        	GL2 gl = canvas.getGL().getGL2();
        	
        	// Setting the clear color -- the color which will be used to erase the canvas.
               
        	gl.glEnable(GL2.GL_NORMALIZE);
                
        	// Create an instance of the model class.
        	this.model = new Model3DS();
        	// Load the object from the file
        	this.model.load("/home/alex/workspace/lab2-gui/objects/geographos.3ds");
        	gl.glClearColor(0, 0, 0, 0);
 
        	// Selecting the modelview matrix.
        	gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
                
        	// Create a new GLU object.
        	glu = GLU.createGLU();
               
         	// Load the texture             
        	this.texture3DS = new TextureHandler(gl, glu, "/home/alex/workspace/lab2-gui/objects/asteroid.bmp", true);
        	texture1 = new TextureHandler(gl, glu, "cat.png", true);
        	gl.glShadeModel(GL2.GL_SMOOTH);
                
        	// Activate the depth test and set the depth function.
        	gl.glEnable(GL.GL_DEPTH_TEST);
        	gl.glDepthFunc(GL.GL_LESS);
        	
        	// Set The Texture Generation Mode For S To Sphere Mapping (NEW)
        	gl.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);                
        	// Set The Texture Generation Mode For T To Sphere Mapping (NEW) 
        	gl.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_SPHERE_MAP);
        	
        	gl.glEnable(GL2.GL_LIGHTING);
        	gl.glEnable(GL2.GL_LIGHT0);
        	gl.glEnable(GL2.GL_LIGHT1);
        	
        	gl.glEnable(GL2.GL_COLOR_MATERIAL);
        	gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);
        }	
        
        // This method is used for updating the vertex coordinates.     
        private void updateCoordinates(){
        	// The lines between BEGIN and END are for moving the billboard. For fixed objects ignore them and just set bbCx, bbCy, bbCz to some values
        	// BEGIN 
        	// this.bbAngle += this.bbAngleDirection * this.bbAngleSpeed;
        	
        	// We have changed the camC* variables so that they point to the actual camera coordinates:
 
        	if (this.bbAngle > 13)
        		this.bbAngleDirection = -1;
        	if (this.bbAngle < -13)
        		this.bbAngleDirection = 1;
 
        	this.bbDistance = 10;
 
        	this.bbCx = Math.cos(Math.toRadians(90 + this.bbAngle)) * this.bbDistance;
        	this.bbCy = 0;
        	this.bbCz = Math.sin(Math.toRadians(90 + this.bbAngle)) * this.bbDistance;
        	// END          
 
        	this.camCx = 0;
        	this.camCy = 0;
        	this.camCz = 0;
 
        	// Compute the billboard vertices based on its center (bbCx, bbCy, bbCz)
        	this.bbV1 = new double[] {this.bbCx - 1, this.bbCy - 1, this.bbCz};
        	this.bbV2 = new double[] {this.bbCx + 1, this.bbCy - 1, this.bbCz};
        	this.bbV3 = new double[] {this.bbCx + 1, this.bbCy + 1, this.bbCz};
        	this.bbV4 = new double[] {this.bbCx - 1, this.bbCy + 1, this.bbCz};
 
        	// Compute the billboard-camera vector. Its x,y,z components will be used to get the angles of rotation
        	double deltaX = this.camCx - this.bbCx;
        	double deltaY = this.camCy - this.bbCy;
        	double deltaZ = this.camCz - this.bbCz;
 
        	// Convert cartesian coordinates to polar coordinates, i.e., get the angles of rotation
        	double alpha = Math.atan2(deltaZ, deltaX) - Math.PI / 2.0;
        	double beta = Math.atan2(deltaY, Math.sqrt(deltaX * deltaX + deltaZ * deltaZ));
 
        	// Perform rotation
        	this.bbV1 = this.rotate(this.bbV1, alpha, beta);
        	this.bbV2 = this.rotate(this.bbV2, alpha, beta);
        	this.bbV3 = this.rotate(this.bbV3, alpha, beta);
        	this.bbV4 = this.rotate(this.bbV4, alpha, beta);
        }
 
        
        public void display(GLAutoDrawable canvas){
        	 GL2 gl = canvas.getGL().getGL2();
        	 
             gl.glClear(GL.GL_COLOR_BUFFER_BIT);
             gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

             // This method is used for updating the vertices.
             // NOTE that it is called BEFORE rendering the quad.            
             this.updateCoordinates();

             gl.glPushMatrix();

             gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);

             this.texture1.bind();
             this.texture1.enable();
             
             gl.glBegin(GL2.GL_QUADS);
             	gl.glTexCoord2d(0, 1);
             	gl.glVertex3dv(this.bbV1, 0);
             	gl.glTexCoord2d(1, 1);
             	gl.glVertex3dv(this.bbV2, 0);
             	gl.glTexCoord2d(1, 0);
             	gl.glVertex3dv(this.bbV3, 0);
             	gl.glTexCoord2d(0, 0);
             	gl.glVertex3dv(this.bbV4, 0);
             gl.glEnd();

             gl.glPopMatrix();

             gl.glFlush();
        }
                
        
        private double[] rotate(double[] v, double alpha, double beta){
                double y = v[1]  * Math.cos(beta) + v[2] * Math.sin(beta);
                double z = -v[1] * Math.sin(beta) + v[2] * Math.cos(beta);
                double x = v[0]  * Math.cos(alpha) - z * Math.sin(alpha);
                z = v[0]  * Math.sin(alpha) + z * Math.cos(alpha);
                return (new double [] {x, y, z});
        }
        
        private void drawScene(GL2 gl){
        	gl.glLoadIdentity();
 
                /*glu.gluLookAt(0.0, 0.0, 5.0,
                        0.0, 0.0, 0.0,
                        0.0, 1.0, 0.0);*/
                // Remember to either add these methods or use gluLookAt(...) here.
                // aimCamera(gl, glu);
                // moveCamera();
 
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
                gl.glTranslated(0, 0, -4);
                
                gl.glPushMatrix();
 
                gl.glScaled(1.0 / 300, 1.0 / 300, 1.0 / 300);
 
                gl.glRotated(this.angle * 1.1, 1, 0, 0);
                gl.glRotated(this.angle * 1.2, 0, 1, 0);
                gl.glRotated(this.angle * 1.3, 0, 0, 1);
 
                //This method has to be implemented by the developer when using JOGLUtils.
                this.renderObject(gl, this.model);

                gl.glPopMatrix();
        }
        
        // Using the model data draw the normals (for lighting) and vertices (+texture coordinates) belonging to the model.
        private void renderObject(GL2 gl, final Model3DS model) 
        {
                for (int objectI = 0; objectI < this.model.getNumberOfObjects(); objectI++) {
                        Obj object = model.getObject(objectI);
 
                        // The texture is read with TextureHandler in init(...)
                        this.texture3DS.bind();
                        this.texture3DS.enable();
                        gl.glBegin(GL2.GL_TRIANGLES);
 
                        for (int faceI = 0; faceI < object.numOfFaces; faceI++) {
 
                                Face face = object.faces[faceI];
 
                                for (int vertexI = 0; vertexI < 3; vertexI++) {
 
                                        Vec3 vertex = object.verts[face.vertIndex[vertexI]];
                                        Vec3 normal = object.normals[face.vertIndex[vertexI]];
 
                                        if (object.hasTexture) {
                                                Vec3 texture = object.texVerts[face.vertIndex[vertexI]];
                                                gl.glTexCoord2d(texture.x, texture.y);
                                        }
                                        gl.glNormal3f(normal.x, normal.y, normal.z);
                                        gl.glColor3f(1.0f, 0.0f, 0.0f);
                                        gl.glVertex3f(vertex.x, vertex.y, vertex.z);
                                }       
                        }
 
                        gl.glEnd();
                        this.texture3DS.disable();
                }
        }
        
        private void pickHandle(GL2 gl, int x, int y) {
            //Calculate the select buffer capacity and allocate data if necessary
            final int bufferSize  = 10;             
            final int capacity = Buffers.SIZEOF_INT * bufferSize;
            IntBuffer selectBuffer = Buffers.newDirectIntBuffer(capacity);

            // Send the select buffer to (J)OGL and use select mode to track object hits.
            gl.glSelectBuffer(selectBuffer.capacity(), selectBuffer);
            gl.glRenderMode(GL2.GL_SELECT);

            // Initialize the name stack.
            gl.glInitNames();

            // Get the viewport.
            int[] viewport = new int[4];
            gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
            // Get the projection matrix.
            float[] projection = new float[16];
            gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, projection, 0);

            // Switch to the projection matrix mode.
            gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
            // Save the current projection matrix.
            gl.glPushMatrix();
            // Reset the projection matrix.
            gl.glLoadIdentity();

            // Restrict region to pick object only in this region.
            // Remember to adjust the y value correctly by taking into consideration the different coordinate systems used by AWT and (J)OGL.
            glu.gluPickMatrix(x, viewport[3] - y, 1, 1, viewport, 0);

            //Load the projection matrix
            gl.glMultMatrixf(projection, 0);

            // Or redefine the perspective again.
            // glu.gluPerspective ( 38.0, (float) screenWidth / (float) screenHeight, 0.1, z_far );

            //Go back to modelview matrix for rendering.
            gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);

            // Render the scene. Note we are in GL_SELECT mode now.
            this.drawScene(gl);
        

            gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
            // Restore the saved projection matrix.  
            gl.glPopMatrix();

            // Select the modelview matrix.
            gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);

            // Return to GL_RENDER mode.
            final int hits = gl.glRenderMode(GL2.GL_RENDER);        
            mode = GL2.GL_RENDER;

            // Process the hits.
            processHits(hits, selectBuffer);
    }
        
        // Retrieved from: http://user.cs.tu-berlin.de/~schabby/PickingExample.java
        // It extracts the data in the selection buffer and writes it on the console.
        private void processHits(int hits, IntBuffer buffer) {
                int offset = 0;
                int names;
                float z1, z2;
 
                System.out.println("---------------------------------");
                System.out.println(" HITS: " + hits);
 
                for (int i = 0; i < hits; i++) {
                        System.out.println("- - - - - - - - - - - -");
                        System.out.println(" hit: " + (i + 1));
                        names = buffer.get(offset);
                        offset++;
                        z1 = (float) buffer.get(offset) / 0x7fffffff;
                        offset++;
                        z2 = (float) buffer.get(offset) / 0x7fffffff;
                        offset++;
                        System.out.println(" number of names: " + names);
                        System.out.println(" z1: " + z1);
                        System.out.println(" z2: " + z2);
                        System.out.println(" names: ");
 
                        for (int j = 0; j < names; j++) {
                                System.out.print("  " + buffer.get(offset));
                                if (j == (names - 1)) {
                                        System.out.println("<-");
                                } else {
                                        System.out.println();
                                }
                                offset++;
                        }
                        System.out.println("- - - - - - - - - - - -");
                }
                System.out.println("--------------------------------- ");
        }
 
      
        public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
        	GL2 gl = canvas.getGL().getGL2();
       	 
            // Selecting the viewport -- the display area -- to be the entire widget.
            gl.glViewport(0, 0, width, height);

            // Determining the width to height ratio of the widget.
            double ratio = (double) width / (double) height;

            // Selecting the projection matrix.
            gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);

            gl.glLoadIdentity();

            glu.gluPerspective (38, ratio, 0.1, 60.0);

            // Selecting the modelview matrix.
            gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW); 
        }
 
        
        public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged){
                return;
        }
 
        
        public void dispose(GLAutoDrawable arg0) {
        	// TODO Auto-generated method stub
        }

		
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void keyPressed(KeyEvent ke) {
			if(ke.getKeyCode() < 250) {
				System.out.println("BLA"+ke.getKeyChar());
                keys[ke.getKeyCode()] = true;
			}
		}

		
		public void keyReleased(KeyEvent ke) {
			System.out.println("REL"+ke.getKeyChar());
	        if (ke.getKeyCode() < 250)
	        	keys[ke.getKeyCode()] = false;
		}


		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
		}


		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		
		public void mousePressed(MouseEvent me) {
			mouseX = me.getX();
			mouseY = me.getY();
			mode = GL2.GL_SELECT;
		}

		
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

}