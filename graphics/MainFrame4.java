import java.io.IOException;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
 
import javax.swing.JFrame;
 
import com.jogamp.opengl.util.Animator;
 
public class MainFrame4 extends JFrame implements GLEventListener {
        private GLCanvas canvas;
        private Animator animator;
        private final int NO_TEXTURES = 2;

    	private int texture[] = new int[NO_TEXTURES];
    	TextureReader.Texture[] tex = new TextureReader.Texture[NO_TEXTURES];

    	// GLU object used for mipmapping.
    	private GLU glu;
 
        // For specifying the positions of the clipping planes (increase/decrease the distance) modify this variable.
        // It is used by the glOrtho method.
        private double v_size = 1.0;
 
        // Default constructor
        public MainFrame4() {
			super("Java OpenGL");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
			this.setSize(800, 600);
			this.initializeJogl();
			this.setVisible(true);
        }
 
        private void initializeJogl() {
			// Creating a new GL profile.
			GLProfile glprofile = GLProfile.getDefault();
			// Creating an object to manipulate OpenGL parameters.
			GLCapabilities capabilities = new GLCapabilities(glprofile);

			// Setting some OpenGL parameters.
			capabilities.setHardwareAccelerated(true);
			capabilities.setDoubleBuffered(true);

			// Try to enable 2x anti aliasing. It should be supported on most hardware.
			capabilities.setNumSamples(2);
			capabilities.setSampleBuffers(true);

			// Creating an OpenGL display widget -- canvas.
			this.canvas = new GLCanvas(capabilities);

			// Adding the canvas in the center of the frame.
			this.getContentPane().add(this.canvas);

			// Adding an OpenGL event listener to the canvas.
			this.canvas.addGLEventListener(this);

			// Creating an animator that will redraw the scene 40 times per second.
			this.animator = new Animator(this.canvas);

			// Starting the animator.
			this.animator.start();
        }
 
        public void init(GLAutoDrawable canvas) {
			// Obtaining the GL instance associated with the canvas.
			GL2 gl = canvas.getGL().getGL2();

			// Create a new GLU object.
			glu = GLU.createGLU();

			// Generate a name (id) for the texture.
			// This is called once in init no matter how many textures we want to generate in the texture vector
		        gl.glGenTextures(NO_TEXTURES, texture, 0);

			// Bind (select) the FIRST texture.
			gl.glBindTexture(GL.GL_TEXTURE_2D, texture[0]);

			// Read the texture from the image.
			try {
				tex[0] = TextureReader.readTexture("/home/alex/workspace/lab2-gui/src/cat2.png");
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}

			// Define the filters used when the texture is scaled.
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);

			// Construct the texture and use mipmapping in the process.
			this.makeRGBTexture(gl, glu, tex[0], GL.GL_TEXTURE_2D, true);
			
			// Bind (select) the SECOND texture.
			gl.glBindTexture(GL.GL_TEXTURE_2D, texture[1]);

			// Read the texture from the image.
			try {
				tex[1] = TextureReader.readTexture("/home/alex/workspace/lab2-gui/src/wall_texture.jpg");

			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}

			// Define the filters used when the texture is scaled.
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);

			// Construct the texture and use mipmapping in the process.
			this.makeRGBTexture(gl, glu, tex[1], GL.GL_TEXTURE_2D, true);

			// Do not forget to enable texturing.
			gl.glEnable(GL.GL_TEXTURE_2D);

        }
 
        public void display(GLAutoDrawable canvas){
                GL2 gl = canvas.getGL().getGL2();
 
        		drawChessBoard(gl);
                //drawTwoSquaresDiffTextures(gl);

                // Forcing the scene to be rendered.
                gl.glFlush();
        }
        
        public void drawChessBoard(GL2 gl){
        	float size = 0.1f;
        	TextureHandler texhandler = new TextureHandler(gl, glu, "/home/alex/workspace/lab2-gui/src/wall_texture.jpg", false);
        	
    		gl.glEnable(GL.GL_CULL_FACE);
    		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);

        	for (int i = 0; i < 8; ++i) {
        	    for (int j = 0; j < 8; ++j) {
        	        if ((i + j) % 2 == 0){
        	    		gl.glCullFace(GL.GL_FRONT);
        	    		texhandler.bind();
        	        	texhandler.enable();
        	        }
        	        else{
        	    		gl.glCullFace(GL.GL_BACK);
						// texhandler.disable();
        	        }

					gl.glBegin(GL2.GL_QUADS);
						gl.glTexCoord2f(0.0f, 0.0f);
						gl.glVertex2f((float) i*size, (float) j*size);

						gl.glTexCoord2f(1.0f, 0.0f);
						gl.glVertex2f((float) (i+1)*size, (float) j*size);

						gl.glTexCoord2f(1.0f, 1.0f);
						gl.glVertex2f((float) (i+1)*size, (float) (j+1)*size);

						gl.glTexCoord2f(0.0f, 1.0f);
						gl.glVertex2f((float) i*size, (float) (j+1)*size);
					gl.glEnd();
        	    }
        	}
        }

        public void drawTwoSquaresDiffTextures(GL2 gl){
        	TextureHandler texture2 = new TextureHandler(gl, glu, "/home/alex/workspace/lab2-gui/src/cat.png", false);
        	TextureHandler texture1 = new TextureHandler(gl, glu, "/home/alex/workspace/lab2-gui/src/wall_texture.jpg", false);
        	
        	texture1.bind();
        	texture1.enable();
        	
        	gl.glDisable(GL.GL_BLEND);
        	
    		gl.glBegin(GL2.GL_QUADS);
    			// Lower left corner.
    			gl.glTexCoord2f(0.0f, 0.0f);
    			gl.glVertex2f(0.1f, 0.1f);

    			// Lower right corner.
    			gl.glTexCoord2f(1.0f, 0.0f);
    			gl.glVertex2f(0.9f, 0.1f);

    			// Upper right corner.
    			gl.glTexCoord2f(1.0f, 1.0f);
    			gl.glVertex2f(0.9f, 0.9f);

    			// Upper left corner.
    			gl.glTexCoord2f(0.0f, 1.0f);
    			gl.glVertex2f(0.1f, 0.9f);
    		gl.glEnd();
    		
    		gl.glEnable(GL.GL_BLEND);

    		// 5 different blending modes:
    		// gl.glBlendFunc(GL.GL_ONE, GL.GL_DST_COLOR);
    		//gl.glBlendFunc(GL.GL_SRC_COLOR, GL.GL_ONE_MINUS_SRC_ALPHA);
    		// gl.glBlendFunc(GL.GL_ONE_MINUS_DST_COLOR, GL.GL_DST_COLOR);
    		// gl.glBlendFunc(GL.GL_ONE_MINUS_SRC_COLOR, GL.GL_ONE_MINUS_SRC_COLOR);
    		// gl.glBlendFunc(GL.GL_ONE_MINUS_SRC_ALPHA, GL.GL_DST_ALPHA);
    		// gl.glBlendFunc(GL.GL_SRC_COLOR, GL.GL_ONE);

    		texture1.disable(); 
        	texture2.bind();
        	texture2.enable();

    		// Draw a square and apply a texture on it.
    		gl.glBegin(GL2.GL_QUADS);
    			// Lower left corner.
    			gl.glTexCoord2f(0.0f, 0.0f);
    			gl.glVertex2f(-0.2f, -0.2f);

    			// Lower right corner.
    			gl.glTexCoord2f(1.0f, 0.0f);
    			gl.glVertex2f(0.6f, -0.2f);

    			// Upper right corner.
    			gl.glTexCoord2f(1.0f, 1.0f);
    			gl.glVertex2f(0.6f, 0.6f);

    			// Upper left corner.
    			gl.glTexCoord2f(0.0f, 1.0f);
    			gl.glVertex2f(-0.2f, 0.6f);
    		gl.glEnd();
        }
        
        public void drawTwoSquares(GL2 gl){
            // Disable blending for this texture.
    		gl.glDisable(GL.GL_BLEND);

    		// Bind (select) the texture
    		gl.glBindTexture(GL.GL_TEXTURE_2D, texture[0]);

    		// Draw a square and apply a texture on it.
    		gl.glBegin(GL2.GL_QUADS);
    			// Lower left corner.
    			gl.glTexCoord2f(0.0f, 0.0f);
    			gl.glVertex2f(0.1f, 0.1f);

    			// Lower right corner.
    			gl.glTexCoord2f(1.0f, 0.0f);
    			gl.glVertex2f(0.9f, 0.1f);

    			// Upper right corner.
    			gl.glTexCoord2f(1.0f, 1.0f);
    			gl.glVertex2f(0.9f, 0.9f);

    			// Upper left corner.
    			gl.glTexCoord2f(0.0f, 1.0f);
    			gl.glVertex2f(0.1f, 0.9f);
    		gl.glEnd();

    		// Enable blending for this texture.
    		gl.glEnable(GL.GL_BLEND);

    		// Set the blend function.
    		gl.glBlendFunc(GL.GL_SRC_COLOR, GL.GL_DST_ALPHA);

    		// Bind (select) the texture
    		gl.glBindTexture(GL.GL_TEXTURE_2D, texture[1]);

    		// Draw a square and apply a texture on it.
    		gl.glBegin(GL2.GL_QUADS);
    			// Lower left corner.
    			gl.glTexCoord2f(0.0f, 0.0f);
    			gl.glVertex2f(0.1f, 0.1f);

    			// Lower right corner.
    			gl.glTexCoord2f(1.0f, 0.0f);
    			gl.glVertex2f(0.9f, 0.1f);

    			// Upper right corner.
    			gl.glTexCoord2f(1.0f, 1.0f);
    			gl.glVertex2f(0.9f, 0.9f);

    			// Upper left corner.
    			gl.glTexCoord2f(0.0f, 1.0f);
    			gl.glVertex2f(0.1f, 0.9f);
    		gl.glEnd();
        }
        
 
        public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height) {
                GL2 gl = canvas.getGL().getGL2();
 
                // Selecting the viewport -- the display area -- to be the entire widget.
                gl.glViewport(0, 0, width, height);
 
                // Determining the width to height ratio of the widget.
                double ratio = (double) width / (double) height;
 
                // Selecting the projection matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
 
                gl.glLoadIdentity();
 
                // Selecting the view volume to be x from 0 to 1, y from 0 to 1, z from -1 to 1.
                // But we are careful to keep the aspect ratio and enlarging the width or the height.
                if (ratio < 1)
                        gl.glOrtho(-v_size, v_size, -v_size, v_size / ratio, -1, 1);
                else
                        gl.glOrtho(-v_size, v_size * ratio, -v_size, v_size, -1, 1);
 
                // Selecting the modelview matrix.
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);             
        }
        
        private void makeRGBTexture(GL gl, GLU glu, TextureReader.Texture img, int target, boolean mipmapped) {     
			if (mipmapped) {
				glu.gluBuild2DMipmaps(target, GL.GL_RGB8, img.getWidth(), img.getHeight(), GL.GL_RGB, GL.GL_UNSIGNED_BYTE, img.getPixels());
			} 
			else {
				gl.glTexImage2D(target, 0, GL.GL_RGB, img.getWidth(), img.getHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, img.getPixels());
			}
		}
 
        public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged)
        {
                return;
        }
 
        public void dispose(GLAutoDrawable arg0) {
                // TODO Auto-generated method stub
        }
}