import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import javax.swing.JFrame;

import com.jogamp.opengl.util.Animator;

import net.java.joglutils.ThreeDS.Model3DS;
import net.java.joglutils.ThreeDS.Obj;
import net.java.joglutils.ThreeDS.Vec3;
import net.java.joglutils.ThreeDS.Face;


public class MainFrame9	extends JFrame implements GLEventListener{
	private Model3DS model;
	private TextureHandler texture3DS;
	
	private GLCanvas canvas;
	private Animator animator;

	private GLU glu;
	float angle = 0.0f;

	public MainFrame9(){
		super("Java OpenGL");
		
		// Registering a window event listener to handle the closing event.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);
		this.initializeJogl();
		this.setVisible(true);
	}
	
	private void initializeJogl(){
		// Creating a new GL profile.
		GLProfile glprofile = GLProfile.getDefault();
		// Creating an object to manipulate OpenGL parameters.
		GLCapabilities capabilities = new GLCapabilities(glprofile);
		
		// Setting some OpenGL parameters.
		capabilities.setHardwareAccelerated(true);
		capabilities.setDoubleBuffered(true);

		// Try to enable 2x anti aliasing. It should be supported on most hardware.
		capabilities.setNumSamples(2);
		capabilities.setSampleBuffers(true);
		
		// Creating an OpenGL display widget -- canvas.
		this.canvas = new GLCanvas(capabilities);
		
		// Adding the canvas in the center of the frame.
		this.getContentPane().add(this.canvas);
		
		// Adding an OpenGL event listener to the canvas.
		this.canvas.addGLEventListener(this);
		
		// Creating an animator that will redraw the scene 40 times per second.
		this.animator = new Animator(this.canvas);
		
		//	Registering the canvas to the animator.
		//	this.animator.add(this.canvas);
		
		// Starting the animator.
		this.animator.start();
	}
	
	
	public void init(GLAutoDrawable canvas){
		// Obtaining the GL instance associated with the canvas.
		GL2 gl = canvas.getGL().getGL2();
		
		// Initialize GLU. We'll need it for perspective and camera setup.
		this.glu = GLU.createGLU();

		// Setting the clear color -- the color which will be used to erase the canvas.
		gl.glClearColor(0, 0, 0, 0);
		
		// Selecting the modelview matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		
		gl.glEnable(GL2.GL_NORMALIZE);

		// Create an instance of the model class.
		this.model = new Model3DS();
		// Load the object from the file
		this.model.load("/home/alex/workspace/lab2-gui/src/objects/geographos.3ds");

		// Load the texture		
		this.texture3DS = new TextureHandler(gl, glu, "/home/alex/workspace/lab2-gui/src/objects/asteroid.bmp", true);
	}
	
	
	public void display(GLAutoDrawable canvas){
		GL2 gl = canvas.getGL().getGL2();
		
		gl.glClear(GL.GL_COLOR_BUFFER_BIT);
		gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
		
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();
		
		gl.glTranslated(0, 0, -4);
		
		gl.glPushMatrix();
			
		gl.glScaled(1.0 / 3, 1.0 / 3, 1.0 / 3);
			
		gl.glRotated(this.angle * 1.1, 1, 0, 0);
		gl.glRotated(this.angle * 1.2, 0, 1, 0);
		gl.glRotated(this.angle * 1.3, 0, 0, 1);
			
		//This method has to be implemented by the developer when using JOGLUtils.
		this.renderObject(gl, this.model);
			
			
		gl.glPopMatrix();
		
		gl.glFlush();
		
		this.angle += 1;
		
		// Forcing the scene to be rendered.
		gl.glFlush();
	}
	
	// Using the model data draw the normals (for lighting) and vertices (+texture coordinates) belonging to the model.
	private void renderObject(GL2 gl, final Model3DS model){
		for (int objectI = 0; objectI < this.model.getNumberOfObjects(); objectI++) {
			Obj object = model.getObject(objectI);
			
			// The texture is read with TextureHandler in init(...)
			this.texture3DS.bind();
			this.texture3DS.enable();
			gl.glBegin(GL2.GL_TRIANGLES);
			
			for (int faceI = 0; faceI < object.numOfFaces; faceI++) {
					
				Face face = object.faces[faceI];
				
				for (int vertexI = 0; vertexI < 3; vertexI++) {
					
					Vec3 vertex = object.verts[face.vertIndex[vertexI]];
					Vec3 normal = object.normals[face.vertIndex[vertexI]];
						
					if (object.hasTexture) {
						Vec3 texture = object.texVerts[face.vertIndex[vertexI]];
						gl.glTexCoord2d(texture.x, texture.y);
					}
					gl.glNormal3f(normal.x, normal.y, normal.z);
					gl.glVertex3f(vertex.x, vertex.y, vertex.z);
				}	
			}
				
			gl.glEnd();
			this.texture3DS.disable();
		}
	}
	
	public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
		GL2 gl = canvas.getGL().getGL2();
		
		// Selecting the viewport -- the display area -- to be the entire widget.
		gl.glViewport(0, 0, width, height);
		
		// Determining the width to height ratio of the widget.
		double ratio = (double) width / (double) height;
		
		// Selecting the projection matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		
		gl.glLoadIdentity();
		
		glu.gluPerspective (38, ratio, 0.1, 100);

		// Selecting the modelview matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);		
	}
	
	
	public void displayChanged(GLAutoDrawable canvas, boolean modeChanged, boolean deviceChanged){
		return;
	}

	
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}
}