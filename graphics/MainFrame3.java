import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;

import javax.swing.JFrame;

import com.jogamp.opengl.util.Animator;
import java.awt.BorderLayout;

public class MainFrame3 extends JFrame implements GLEventListener
{
	private GLCanvas canvas;
	private Animator animator;
	double equation[] = { 1f, 1f, 1f, 1f};
	int aCircle;

	byte mask[] = {
        	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	        0x03, (byte)0x80, 0x01, (byte)0xC0, 0x06, (byte)0xC0, 0x03, 0x60, 

	        0x04, 0x60, 0x06, 0x20, 0x04, 0x30, 0x0C, 0x20, 
	        0x04, 0x18, 0x18, 0x20, 0x04, 0x0C, 0x30, 0x20,
	        0x04, 0x06, 0x60, 0x20, 0x44, 0x03, (byte)0xC0, 0x22, 
	        0x44, 0x01, (byte)0x80, 0x22, 0x44, 0x01, (byte)0x80, 0x22, 
	        0x44, 0x01, (byte)0x80, 0x22, 0x44, 0x01, (byte)0x80, 0x22,
	        0x44, 0x01, (byte)0x80, 0x22, 0x44, 0x01, (byte)0x80, 0x22, 
	        0x66, 0x01, (byte)0x80, 0x66, 0x33, 0x01, (byte)0x80, (byte)0xCC, 

	        0x19, (byte)0x81, (byte)0x81, (byte)0x98, 0x0C, (byte)0xC1, (byte)0x83, 0x30,
	        0x07, (byte)0xe1, (byte)0x87, (byte)0xe0, 0x03, 0x3f, (byte)0xfc, (byte)0xc0, 
	        0x03, 0x31, (byte)0x8c, (byte)0xc0, 0x03, 0x33, (byte)0xcc, (byte)0xc0, 
	        0x06, 0x64, 0x26, 0x60, 0x0c, (byte)0xcc, 0x33, 0x30,
	        0x18, (byte)0xcc, 0x33, 0x18, 0x10, (byte)0xc4, 0x23, 0x08, 
	        0x10, 0x63, (byte)0xC6, 0x08, 0x10, 0x30, 0x0c, 0x08, 
	        0x10, 0x18, 0x18, 0x08, 0x10, 0x00, 0x00, 0x08};
	
	public MainFrame3()
	{
		super("Java OpenGL");
		
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setSize(800, 600);
		
		// This method will be explained later
		this.initializeJogl();
		
		this.setVisible(true);
	}

	private void initializeJogl() {
		// Obtaining a reference to the default GL profile
		GLProfile glProfile = GLProfile.getDefault();
		// Creating an object to manipulate OpenGL parameters.
		GLCapabilities capabilities = new GLCapabilities(glProfile);
		
		// Setting some OpenGL parameters.
		capabilities.setHardwareAccelerated(true);
		capabilities.setDoubleBuffered(true);
		
		// Creating an OpenGL display widget -- canvas.
		this.canvas = new GLCanvas();
		
		// Adding the canvas in the center of the frame.
		this.getContentPane().add(this.canvas);
		
		// Adding an OpenGL event listener to the canvas.
		this.canvas.addGLEventListener(this);
		
		this.animator = new Animator();
		this.animator.add(this.canvas);
		this.animator.start();
	}

	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}

	public void init(GLAutoDrawable canvas) {
		// Obtain the GL instance associated with the canvas.
		GL2 gl = canvas.getGL().getGL2();
		
		// Set the clear color -- the color which will be used to reset the color buffer.
		gl.glClearColor(0, 0, 0, 0);
		
		// Select the Projection matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		
		// Clear the projection matrix and set it to be the identity matrix.
		gl.glLoadIdentity();
		
		// Set the projection to be orthographic.
		// It could have been as well chosen to be perspective.
		// Select the view volume to be x in the range of 0 to 1, y from 0 to 1 and z from -1 to 1. 
		gl.glOrtho(0, 1, 0, 1, -1, 1);	
		
		// Enable additional Clip plane.
		gl.glEnable( GL2.GL_CLIP_PLANE1 );

		// Set clip plane to be defined by the 	equation arguments
		gl.glClipPlane( GL2.GL_CLIP_PLANE1 , equation, 0 );

		// Activate the GL_LINE_SMOOTH state variable. Other options include 
		// GL_POINT_SMOOTH and GL_POLYGON_SMOOTH.
		gl.glEnable(GL.GL_LINE_SMOOTH);

		// Activate the GL_BLEND state variable. Means activating blending.
		//gl.glEnable(GL.GL_BLEND);

		// Set the blend function. For antialiasing it is set to GL_SRC_ALPHA for the source
		// and GL_ONE_MINUS_SRC_ALPHA for the destination pixel.
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		// Control GL_LINE_SMOOTH_HINT by applying the GL_DONT_CARE behavior. 
		// Other behaviours include GL_FASTEST or GL_NICEST.
		gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_DONT_CARE);
		// Uncomment the following two lines in case of polygon antialiasing
		//gl.glEnable(GL.GL_POLYGON_SMOOTH);
		//glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		

		// Generate a unique ID for our list.
		aCircle = gl.glGenLists(1);

		// Generate the Display List
		gl.glNewList(aCircle, GL2.GL_COMPILE);
			drawCircle(gl, 0.5f, 0.5f, 0.4f);
		gl.glEndList();
	}

	public void reshape(GLAutoDrawable canvas, int left, int top, int width, int height){
		GL2 gl = canvas.getGL().getGL2();
		
		// Select the viewport -- the display area -- to be the entire widget.
		gl.glViewport(0, 0, width, height);
		
		// Determine the width to height ratio of the widget.
		double ratio = (double) width / (double) height;
		
		// Select the Projection matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		
		gl.glLoadIdentity();
		
		// Select the view volume to be x in the range of 0 to 1, y from 0 to 1 and z from -1 to 1.
		// We are careful to keep the aspect ratio and enlarging the width or the height.
		if (ratio < 1)
			gl.glOrtho(0, 1, 0, 1 / ratio, -1, 1);
		else
			gl.glOrtho(0, 1 * ratio, 0, 1, -1, 1);

		// Return to the Modelview matrix.
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
	}
	
	public void display(GLAutoDrawable canvas) {
		GL2 gl = canvas.getGL().getGL2();

		// drawSquare(gl);
		// drawPoligon(gl);
		// drawPoligon2(gl);
		// drawPoligonBack(gl);
		// drawPoligonStipple(gl);
		//drawPoligonNormala(gl);
		
		gl.glColor3f(1.0f, 1.0f, 1.0f);
		// Call the Display List i.e. call the commands stored in it.
		gl.glCallList(aCircle);
		
		gl.glFlush();
	}
	
	// Here we define the function for building a circle from line segments.
	private void drawCircle(GL2 gl, float xCenter, float yCenter, float radius) {

		double x,y, angle;

		gl.glBegin(GL2.GL_LINE_LOOP);
		for (int i=0; i<360; i++) {
			angle = Math.toRadians(i);
			x = radius * Math.cos(angle);
			y = radius * Math.sin(angle);
			gl.glVertex2d(xCenter + x, yCenter + y);
		}
		gl.glEnd();
	}
	
	public void drawPoligonNormala(GL2 gl){
		// Do not render front-faced polygons.
		gl.glCullFace(GL2.GL_FRONT);
		// Culling must be enabled in order to work.
		gl.glEnable(GL2.GL_CULL_FACE);		

		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);

		// Define vertices in clockwise order (back-faced).
		gl.glBegin(GL2.GL_POLYGON);
			// Define normal for vertex 1
			gl.glNormal3f(0.f, 0.f, 1.f);
			gl.glColor3f(1.f, 0.f, 0.f);
			gl.glVertex2f(0.2f, 0.2f);

			// Define normal for vertex 2
			gl.glNormal3f(0.f, 0.f, 1.f);
			gl.glColor3f(0.f, 1.f, 0.f);
			gl.glVertex2f(0.2f, 0.4f);

			// Define normal for vertex 3
			gl.glNormal3f(0.f, 0.f, 1.f);
			gl.glColor3f(0.f, 0.f, 1.f);
			gl.glVertex2f(0.4f, 0.4f);

			// Define normal for vertex 4
			gl.glNormal3f(0.f, 0.f, 1.f);
			gl.glColor3f(1.f, 1.f, 1.f);
			gl.glVertex2f(0.4f, 0.2f);
		gl.glEnd();
	}
	
	public void drawPoligonStipple(GL2 gl){
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);

		// Set the polygon mask.
		gl.glPolygonStipple (mask, 0);
		// Enable polygon stipple.
		gl.glEnable (GL2.GL_POLYGON_STIPPLE);

		// Define vertices in clockwise order (back-faced).
		gl.glBegin(GL2.GL_POLYGON);
			gl.glColor3f(1.f, 0.f, 0.f);
			gl.glVertex2f(0.2f, 0.2f);
			gl.glColor3f(0.f, 1.f, 0.f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.f, 0.f, 1.f);
			gl.glVertex2f(0.4f, 0.4f);
			gl.glColor3f(1.f, 1.f, 1.f);
			gl.glVertex2f(0.4f, 0.2f);
		gl.glEnd();

		// Disable polygon stipple.
		gl.glDisable (GL2.GL_POLYGON_STIPPLE);
	}
	
	public void drawPoligonBack(GL2 gl){
		gl.glCullFace(GL2.GL_BACK);
		// Culling must be enabled in order to work.
		gl.glEnable(GL2.GL_CULL_FACE);		

		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);

		// Define vertices in clockwise order (back-faced).
		gl.glBegin(GL2.GL_POLYGON);
			gl.glColor3f(1.f, 0.f, 0.f);
			gl.glVertex2f(0.2f, 0.2f);
			gl.glColor3f(0.f, 1.f, 0.f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.f, 0.f, 1.f);
			gl.glVertex2f(0.4f, 0.4f);
			gl.glColor3f(1.f, 1.f, 1.f);
			gl.glVertex2f(0.4f, 0.2f);
		gl.glEnd();
	}
	
	public void drawPoligon2(GL2 gl){
		// Do not render front-faced polygons.
		gl.glCullFace(GL2.GL_FRONT);
		// Culling must be enabled in order to work.
		gl.glEnable(GL2.GL_CULL_FACE);		

		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);

		// Define vertices in clockwise order (back-faced)
		gl.glBegin(GL2.GL_POLYGON);
			gl.glColor3f(1.f, 0.f, 0.f);
			gl.glVertex2f(0.2f, 0.2f);
			gl.glColor3f(0.f, 1.f, 0.f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.f, 0.f, 1.f);
			gl.glVertex2f(0.4f, 0.4f);
			gl.glColor3f(1.f, 1.f, 1.f);
			gl.glVertex2f(0.4f, 0.2f);
		gl.glEnd();
	}
	
	public void drawPoligon(GL2 gl){
		gl.glBegin(GL2.GL_POLYGON);
			gl.glColor3f(1.f, 0.f, 0.f);
			gl.glVertex2f(0.2f, 0.2f);
			gl.glColor3f(0.f, 1.f, 0.f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.f, 0.f, 1.f);
			gl.glVertex2f(0.4f, 0.4f);
			gl.glColor3f(1.f, 1.f, 1.f);
			gl.glVertex2f(0.4f, 0.2f);
		gl.glEnd();
	}
	
	public void drawSquare(GL2 gl){
		gl.glEnable(GL2.GL_LINE);
		
		gl.glLineWidth(8.9f);
	
		gl.glColor3f(1.f, 0.f, 0.f);
		gl.glBegin(GL2.GL_LINES);
			gl.glVertex2f(0.2f, 0.2f);
			gl.glVertex2f(0.9f, 0.9f);			
		gl.glEnd();
		
		gl.glColor3f(0.f, 1.f, 0.f);
		gl.glBegin(GL2.GL_LINES);
			gl.glVertex2f(0.9f, 0.2f);
			gl.glVertex2f(0.2f, 0.9f);
		gl.glEnd();

		/*
		gl.glLineWidth(1.9f);

		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(1.0f, 0.0f, 0.0f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.0f, 1.0f, 0.0f);
			gl.glVertex2f(0.2f, 0.8f);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(0.0f, 0.0f, 1.0f);
			gl.glVertex2f(0.2f, 0.4f);
			gl.glColor3f(0.9f, 0.9f, 0.0f);
			gl.glVertex2f(0.8f, 0.4f);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(0.1f, 0.5f, 0.9f);
			gl.glVertex2f(0.2f, 0.8f);
			gl.glColor3f(0.9f, 0.2f, 0.0f);
			gl.glVertex2f(0.8f, 0.8f);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
			gl.glColor3f(0.0f, 0.2f, 0.2f);
			gl.glVertex2f(0.8f, 0.8f);
			gl.glColor3f(0.5f, 0.9f, 0.5f);
			gl.glVertex2f(0.8f, 0.4f);
		gl.glEnd();
		*/
		
		gl.glDisable(GL2.GL_LINE);
	}
}