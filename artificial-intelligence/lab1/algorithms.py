from collections import defaultdict 

visited = []

class Graph: 
    def __init__(self): 
        self.graph = defaultdict(list)
  
    def addEdge(self, node, val):
        self.graph[node].append(val) 
  
    def depthFirstSearch(self,startnode, val): 
        if(startnode in visited):
            pass
        else:
            visited.append(startnode)
        if(startnode == val):
            return True
  
        for i in self.graph[startnode]: 
            if(self.depthFirstSearch(i, val)): 
                return True

        return False

    def depthLimitedSearch(self, startnode, val, l): 
        if(startnode in visited):
            pass
        else:
            visited.append(startnode)

        if(startnode == val):
            return True
  
        if l < 0 :
            return False
  
        for i in self.graph[startnode]: 
            if(self.depthLimitedSearch(i, val, l-1)): 
                return True

        return False

    def iterativeDeepeningSearch(self, startnode,  val, maxl): 
        for i in range(maxl): 
            if (self.depthLimitedSearch(startnode, val, i)): 
                return True
        return False
  

g = Graph()
g.addEdge(0, 1) 
g.addEdge(0, 2) 
g.addEdge(1, 3) 
g.addEdge(1, 4) 
g.addEdge(2, 5) 
g.addEdge(2, 6) 
g.addEdge(4, 7)


# print("Result: {0},\nVisited list: {1}".format(g.depthFirstSearch(0, 6), visited))
# print("Result: {0},\nVisited list: {1}".format(g.depthLimitedSearch(0, 7, 2), visited))
print("Result: {0},\nVisited list: {1}".format(g.iterativeDeepeningSearch(0, 3, 0), visited))
