import turtle

def dreapta(x1,y1,x2,y2):
    window = turtle.Screen()
    window.title("Desenare dreapta")
    creion = turtle.Turtle()
    creion.penup()
    creion.write("P1")
    creion.pendown()
    creion.goto(x2,y2)
    creion.write("P2")
    window.exitonclick()

dreapta(10,100,50,72)
