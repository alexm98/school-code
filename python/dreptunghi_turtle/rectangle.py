import turtle

l1 = input("Inaltime = ")
l2 = input("Latime = ")

perimetru = l1*2+l2*2
aria = l1*l2

def desenare_dreptunghi():
    window = turtle.Screen()
    window.title("Desenare dreptunghi")
    creion = turtle.Turtle()
    creion.speed(4) 
    creion.pendown()

    for x in range(0,2):
        creion.forward(l2)
        creion.right(90)
        creion.forward(l1)
        creion.right(90)
    
    creion.penup()

    #afisare arie & perimetru
    
    creion.hideturtle()
    creion.left(90)
    creion.forward(50)
    creion.write("Aria = " + str(aria))
    creion.forward(10)
    creion.write("Perimetrul = " + str(perimetru))
    window.exitonclick()

desenare_dreptunghi()
