<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="lab3.xsl"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
  <xsl:template match="/crawldata">
    <html>
      <head>
	<title>Crawled websites</title>
      </head>
      <body>
	<table border="1">
	  <thead>
	    <tr>
	      <td>Title</td>
	      <td>URL</td>
	      <td>Crawled Date</td>
	      <td>Header Info</td>
	      <td></td>
	      <td></td>
	    </tr>
	  </thead>
	  <xsl:for-each select="./website">
	    <tr>
	      <td><strong><xsl:value-of select="./title" /></strong></td>
	      <td><a><xsl:attribute name="href"> <xsl:value-of select="./url" /> </xsl:attribute> <xsl:value-of select="./url" /></a></td>
	      <td><xsl:value-of select="./datecrawled"/></td>

	      <xsl:for-each select="./headerinfo">
		<td><xsl:value-of select="./ssl"/></td>
		<td><xsl:value-of select="./content-type"/></td>
		<td><xsl:value-of select="./content-length"/></td>
	      </xsl:for-each>
	    </tr>
	  </xsl:for-each>
	</table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
