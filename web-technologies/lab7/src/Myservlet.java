

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Myservlet
 */
@WebServlet("/Myservlet")
public class Myservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Myservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/script.js");
		dispatcher.include(request, response);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String title = "Form for something";
		out.println("<html><head><script src=\"script.js\"></script></head><body bgcolor=\"#FDF5E6\">\n" + "<h1 align=\"center\">" + title + "</h1>\n");
		out.println("<form action=\"#\"><label for=\"username\">Username:</label><br/><input type=\"text\" name=\"username\"/></form>\n");
		out.println("<label for=\"firstname\">First Name: </label><br/><input type=\"text\" name=\"firstname\"/><br/>");
		out.println("<label for=\"lastname\">Last Name: </label><br/><input type=\"text\" name=\"lastname\"/><br/>");
		out.println("<label for=\"username\">Username: </label><br/><input type=\"text\" name=\"username\"/><br/>");
		out.println("<label for=\"passwd\">Password: </label><br/><input type=\"password\" name=\"passwd\"/><br/>");
		out.println("<label for=\"confirmpasswd\">Confirm Password: </label><br/><input type=\"password\" name=\"confirmpasswd\"/><br/>");
		out.println("<label for=\"address\">Address: </label><br/><input type=\"text\" name=\"address\"/><br/>");
		out.println("<label for=\"interests\">Interests: </label><br/><textarea name=\"interests\"></textarea><br/><br/>");
		out.println("<button id=\"clearform\">Clear Form</button>");
		out.println("<input type=\"submit\"></input></form>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
