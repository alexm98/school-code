import os
import shutil
from flask import Flask, flash, request, redirect, url_for, jsonify
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.config["SECRET_KEY"] = "1234"
app.config["STORAGE_DIR"] = "./artefacts"

ALLOWED_EXTENSIONS = set(["txt", "pdf", "png", "jpg", "jpeg", "gif"])

def allowed_file(filename):
    return "." in filename and \
           filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/artefacts", methods=["GET"])
def getdirs():
    return jsonify({"directories" : os.listdir("./artefacts")})

@app.route("/artefacts/<directory>", methods=["GET"])
def get_artifacts(directory):
    return jsonify(os.listdir("./artefacts/" + directory))

@app.route("/artefacts/<directory>/<id>", methods=["GET","POST"])
def add_artifact(directory,id):
    path = os.path.join(app.config["STORAGE_DIR"],directory,id)

    if request.method == "POST":
         if "file" not in request.files:
            flash("No file part")
            return redirect(request.url)
         else:
            file = request.files["file"]

            if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(path)
                    return redirect(url_for("add_artifact", directory=directory, id=id))

    if request.method == "GET":
        if os.path.exists(path):
            return f"The {path} file exists, I should have displayed the content here."
        else:
            return """
            <!doctype html>
            <title>Upload new File</title>
            <h1>Upload new File</h1>
            <form method=post enctype=multipart/form-data>
            <input type=file name=file>
            <input type=submit value=Upload>
            </form>
            """


@app.route("/artefacts/<directory>/<id>", methods=["DELETE"])
def delete_artifact(directory,id):
    path = os.path.join(app.config["STORAGE_DIR"],directory,id)

    if os.path.exists(path):
        os.remove(path)
        return "OK"

    return "Path does not exist."


@app.route("/artefacts/<directory>", methods=["DELETE"])
def delete_dir(directory):
    path = os.path.join(app.config["STORAGE_DIR"],directory)

    if os.path.exists(path):
        shutil.rmtree(path)
        return "OK"

    return "Path does not exist."

@app.route("/artefacts/<directory>/<id>", methods=["PUT"])
def put_artifact(directory,id):
    path = os.path.join(app.config["STORAGE_DIR"],directory,id)


    if request.method == "PUT":
        if os.path.exists(path):
            os.remove(path)

            if "file" not in request.files:
                flash("No file part")
                return redirect(request.url)

            else:
                file = request.files["file"]

            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(path)
                return redirect(url_for("add_artifact", directory=directory, id=id))
        return "OK"

    return "Artifact does not exist."

if __name__ == "__main__":
    app.run()
