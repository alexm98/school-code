import org.xml.sax.*;
import org.xml.sax.helpers.XMLReaderFactory;
import java.io.IOException;


public class SAXExample {
	public static void main(String[] args) throws IOException {
		try {
			XMLReader parser = XMLReaderFactory.createXMLReader();
			ContentHandler handler = new TextExtractor();
			parser.setContentHandler(handler);

			parser.parse(args[0]);
			System.out.println(args[0] + " is well-formed.");
		}
		catch (SAXException e) {
			System.out.println(args[0] + " is not well-formed.");
		}
	}

}