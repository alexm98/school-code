import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class DOMCreatorExample {

	public static void main(String[] av) throws IOException, TransformerException {
		DOMCreatorExample dc = new DOMCreatorExample();
		Document doc = dc.makeXML();

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		StreamResult result = new StreamResult(new File("/home/alex/faculta/an3/web-technologies/lab4/output.xml"));
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
	}

	public Document makeXML() {
		try {
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder parser = fact.newDocumentBuilder();

			Document doc = parser.parse(new FileInputStream(new File("/home/alex/faculta/an3/web-technologies/lab3/lab3.xml")));

			return doc;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}