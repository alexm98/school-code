function validateForm(){
    var nrofintegers = document.getElementById("integernr").value;
    var min = document.getElementById("min").value;
    var max = document.getElementById("max").value;
    var columns = document.getElementById("columns").value;
    
    if(nrofintegers != ""){
	if(isNaN(nrofintegers)){
	    alert("Please only enter an integer");
	}
	else if(!(nrofintegers < 100 && nrofintegers > 0)){
	    alert("Integer range should be 0 - 100");
	}
    }

    if(min != ""){
	if(isNaN(min)){
	    alert("Please only enter an integer");
	}
	else if(!(min < 100 && min > 0)){
	    alert("Integer range should be 0 - 100");
	}
    }

    if(max != ""){
	if(isNaN(max)){
	    alert("Please only enter an integer");
	}
	else if(!(max < 100 && max > 0)){
	    alert("Integer range should be 0 - 100");
	}
    }

    if(columns != ""){
	if(isNaN(columns)){
	    alert("Please only enter an integer");
	}
	else if(!(columns < 100 && columns > 0)){
	    alert("Integer range should be 0 - 100");
	}
    }

    return false;
}
