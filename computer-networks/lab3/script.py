class File:
    def __init__(self,fname):
        self.fl = open(fname,'a+r')

    def send_byte(self,msg):
        self.fl.write(msg)

    def read_byte(self):
        return self.fl.read(1)

class Encoder:
    global flag
    flag = "$"
    global esc
    esc = "\\"

    def __init__(self):
        self.f = File('proto.txt')

    def encode(self,msg):
        for i in msg:
            if i == esc:
                self.f.send_byte(esc)
                self.f.send_byte(esc)
            elif i == flag:
                self.f.send_byte(esc)
                self.f.send_byte(flag)
            else:
                self.f.send_byte(i)

        self.f.send_byte(flag)

class Decoder:
    global flag
    flag = "$"
    global esc
    esc = "\\"

    def __init__(self):
        self.f = File('proto.txt')

    def decode(self):
        packet = ''
        while 1:
            byte = self.f.read_byte()

            if not byte:
                return packet
            if byte == esc:
                byte = self.f.read_byte()
                packet += byte
                continue
            if byte == flag:
                return packet

            packet += byte

        return packet


encoder = Encoder()
encoder.encode('salut')
encoder.encode('timisoara$')
encoder.encode('timisoara\\')
encoder.encode('timisoara$$')
decoder = Decoder()
print decoder.decode()
print decoder.decode()
