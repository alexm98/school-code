#include <iostream>
#include <ctime>

using namespace std;

void modificaredata(char *str ,struct tm * data){
    char *s = strptime(str, " %d/%m/%Y ",data);
    const time_t zi = 24*60*60;
    time_t data_secunde = mktime(data)+(5*zi);
    *data = *(localtime(&data_secunde));
}

int main()
{
    struct tm data={1,5,12};
    modificaredata("5/10/2015",&data);
    cout <<asctime(&data)<< endl;
    return 0;
}
