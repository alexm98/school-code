#include <iostream>
#include <new>
#include <stdlib.h>

using namespace std;

void no_mem();
    int main(){
    int *ip;
    long dim;
    set_new_handler(&no_mem);
    cout<<"Dimensiune bloc : ";
    cin>>dim;
    for(int i=1;;i++){
        ip = new int[dim];
        cout<<"Alocare bloc i = "<<i<<endl;
    }
    return 0;
}

void no_mem(){
    cout<<"Alocare imposibila\n";
    exit(1);
}
