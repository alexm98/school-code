#include<iostream>

using namespace std;

class Elicopter{
    public:
        string numeModel;
        string firmaProducatoare;
        int viteza_max;
        int inaltime_max;
        double pret;
        friend ostream & operator >> (ostream &, Elicopter&);
        friend void modificare(Elicopter &x,string nume_nou, double pret_nou);
};

// supraincarcare pentru operator de scriere
ostream & operator << (ostream&, Elicopter x ){
    cout<<"Model : "<<x.numeModel<<endl;
    cout<<"Firma : "<<x.firmaProducatoare<<endl;
    cout<<"Viteza Maxima : "<<x.viteza_max<<endl;
    cout<<"Inaltime Maxima : "<<x.inaltime_max<<endl;
    cout<<"Pretul : "<<x.pret<<endl;    
}
// clasa prietena pentru modificarea modelului si a pretului
void modificare(Elicopter &x,string nume_nou, double pret_nou){
    x.numeModel = nume_nou;
    x.pret = pret_nou;    
}

void introducere_date(Elicopter &x,string model,string producator,int viteza,int inaltime,double pret){
    x.numeModel = model;
    x.firmaProducatoare = producator;
    x.viteza_max = viteza;
    x.inaltime_max = inaltime;
    x.pret = pret;
}

void best(Elicopter a[]){
    Elicopter tmp;
    for(int i=0;i<2;i++){
         if((a[i].pret/a[i].viteza_max) > (tmp.pret/tmp.viteza_max))
             tmp = a[i];
            
    }
    cout<<"Cel mai bun elicopter : "<<endl;
    cout<<tmp;
}
Elicopter elicoptere[10];

//trebuie implementata functia de statistica
void statistica(Elicopter a[]){
    string firme[50];
    for(int i=0;i<=10;i++){
        firme[i] = elicoptere[i].firmaProducatoare;
    }
}

int main(){
    Elicopter elicopter1;
    introducere_date(elicopter1,"X10","ABC",220,1500,35.99);
    introducere_date(elicoptere[0],"T20","DEF",240,1700,59.640);
    introducere_date(elicoptere[1],"X120","XYZ",200,1500,32.21);
    introducere_date(elicoptere[1],"B64","HIJ",190,1000,25.43);

    //print_date(elicopter1);
    cout<<elicopter1;

    for(int i=0;i<3;i++){
        cout<<"Elicopter "<<i<<":"<<endl;
        cout<<elicoptere[i];
    } 
    best(elicoptere);
    cout<<"Dupa modificare:"<<endl;
    modificare(elicoptere[0],"Modelnou",1500.20);
    cout<<elicoptere[0];   
    statistica(elicoptere);   
    return 0;   
}
