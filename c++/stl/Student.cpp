#include "Student.h"
#include<iostream>
#include<string.h>

using namespace std;

Student::Student(string nume, int varsta, int anstudiu, bool bursa)
{
    this->nume = nume;
    this->varsta = varsta;
    this->anstudiu = anstudiu;
    this->bursa = bursa;
}

void Student::afisare()
{
    cout<<this->nume<<" "<<this->varsta<<" "<<this->anstudiu<<" "<<this->bursa<<endl;
}

//functie care defineste daca are sau nu bursa studentul
bool Student::getbursa(){
    return bursa;
}

//supraincarcare operator de scriere
ostream & operator<<(ostream &out, Student &x){
    out<<x.nume<<" "<<x.varsta<<" "<<x.anstudiu<<" "<<x.bursa<<endl;
    return out;
}

Student::Student()
{

}

Student::~Student()
{
    //dtor
}
