#ifndef STUDENT_H
#define STUDENT_H
#include <string.h>
#include <iostream>

using namespace std;

class Student
{
    public:
        string nume;
        int varsta;
        int anstudiu;
        bool bursa;

        Student(string nume, int varsta, int anstudiu, bool bursa);
        Student();
        virtual ~Student();
        void afisare();
        bool getbursa();
        friend ostream & operator<<(ostream &out, Student &x);
    protected:
    private:
};

#endif // STUDENT_H
