#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include "Student.cpp"
#include "Student.h"

using namespace std;

void afisare_student(Student x){
    x.afisare();
}
/*
int numarare(vector <Student>){
    int numar;
    if(x.getbursa())
        numar++;
    return numar;
}*/

int main()
{
    //declarare vector de tipul clasei Student care contine obiecte
    vector<Student>v;
    v.push_back(Student("Student1",19,2,true));
    v.push_back(Student("Student2",20,3,false));
    v.push_back(Student("Student3",18,1,true));

    //Student x("test",19,2,true);
    //x.afisare();

    //afisare cu for_each
    //for_each(v.begin(),v.end(),afisare_student);

    //afisare cu iterator
    for(vector<Student>::iterator i=v.begin();i!=v.end();i++)
        cout<<*i;

    return 0;
}
