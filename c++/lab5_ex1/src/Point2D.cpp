#include <iostream>
#include <math.h>
#include "Point2D.h"

int Point2D::count = 0;

Point2D::Point2D()
{
    this->x = this->y = 0;
    count++;
}

Point2D::Point2D(double x, double y)
{
    this->x = x;
    this->y = y;
    count++;
}

Point2D::~Point2D()
{
    count--;
}

int Point2D::getCount()
{
    return count;
}

void Point2D::afisare() const
{
    std::cout<< '(' << this->x << ", "  << this->y << ')';
}

void Point2D::translate(const int a, const int b)
{
    if (this->validCoordonate(this->x+a)) this->x+=a;
    if (this->validCoordonate(this->y+b)) this->y+=b;
}

double distance(const Point2D &a, const Point2D &b)
{
    return sqrt ((a.x-b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}

bool Point2D::equals(const Point2D&b, const Point2D &a)
{
    return (a.getX() == b.getX()) && (a.getY() == b.getY());
}


