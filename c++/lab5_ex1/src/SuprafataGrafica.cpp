#include "SuprafataGrafica.h"
#include<iostream>
#include<string.h>
using namespace std;

SuprafataGrafica::SuprafataGrafica()
{
    this->indexPuncte=0;
    this->indexLinii =0;
    this->indexTriunghiuri=0;

    this->sirPuncte = new Point2D*[SuprafataGrafica::DIM_SIR];
    this->sirLinii = new Linie*[SuprafataGrafica::DIM_SIR];
    this->sirTriunghiuri = new Triunghi*[SuprafataGrafica::DIM_SIR];
}

SuprafataGrafica::~SuprafataGrafica()
{
    if (this->sirPuncte!=nullptr)
    {
        for (int i=0; i<indexPuncte; i++)
            delete sirPuncte[i];
        delete []sirPuncte;
    }
    if (this->sirLinii!=nullptr)
    {
        for (int i=0; i<indexLinii; i++)
            delete sirLinii[i];
        delete []sirLinii;
    }
    if (this->sirTriunghiuri!=nullptr)
    {
        for (int i=0; i<indexTriunghiuri; i++)
            delete sirTriunghiuri[i];
        delete []sirTriunghiuri;
    }
}

void SuprafataGrafica::afisare(TIP_FIGURA t) const
{
    switch(t)
    {
    case PUNCT:
        for (int i=0; i<this->indexPuncte; i++)
            {
                sirPuncte[i]->Point2D::afisare();
            }
            break;
    case LINIE:
        for (int i=0; i<this->indexLinii; i++)
            {
                //sirLinii[i]->Linie::afisare();
                //STERGETI // DUPA CE IMPLEMENTATI CLASA LINIE
            }
            break;
    case TRIUNGHI:
        for (int i=0; i<this->indexTriunghiuri; i++)
            {
                sirTriunghiuri[i]->Triunghi::afisare();
            }
            break;
    };
}

void SuprafataGrafica::addFigura(TIP_FIGURA t, int *coordonate, const char *culoare)
{
    switch(t)
    {
    case PUNCT:
        if (indexPuncte % SuprafataGrafica::DIM_SIR == 0)
        {
            Point2D **s = new Point2D*[this->indexPuncte+DIM_SIR];
            for (int i=0; i<this->indexPuncte; i++)
            {
                s[i]=this->sirPuncte[i];
            }
            delete []sirPuncte;
            sirPuncte = s;
        }
        this->sirPuncte[this->indexPuncte] = new Point2D(coordonate[0],coordonate[1]);
        indexPuncte++;
        break;
    case LINIE:
        /*
        if (indexLinii % SuprafataGrafica::DIM_SIR == 0)
        {
            Linie **s = new Linie*[this->indexLinii+DIM_SIR];
            for (int i=0; i<this->indexLinii; i++)
            {
                s[i]=this->sirLinii[i];
            }
            delete []sirLinii;
            sirLinii = s;
        }
        this->sirLinii[this->indexLinii] = new Linie(coordonate[0],coordonate[1],coordonate[2],coordonate[3], culoare);
        indexLinii++;
        break;
        */
        //STERGETI /* si */ DUPA CE IMPLEMENTATI CLASA LINIE
    case TRIUNGHI:
        if (indexTriunghiuri % SuprafataGrafica::DIM_SIR == 0)
        {
            Triunghi **s = new Triunghi*[this->indexTriunghiuri+DIM_SIR];
            for (int i=0; i<this->indexTriunghiuri; i++)
            {
                s[i]=this->sirTriunghiuri[i];
            }
            delete []sirTriunghiuri;
            sirTriunghiuri = s;
        }
        this->sirTriunghiuri[this->indexTriunghiuri] = new Triunghi(coordonate[0],coordonate[1],coordonate[2],coordonate[3],coordonate[4],coordonate[5], culoare);
        indexTriunghiuri++;
        break;
    };
}

void SuprafataGrafica::statistica(const SuprafataGrafica& sg)
{
    int rosu = 0;
    int verde = 0;
    int albastru = 0;
    int negru = 0;
    int alb = 0;
    for (int i = 0; i<sg.indexTriunghiuri; i++) {
        if (strcmp(sg.sirTriunghiuri[i]->Triunghi::getCuloare(), "rosu") == 0)
            rosu++;
        if (strcmp(sg.sirTriunghiuri[i]->Triunghi::getCuloare(), "verde") == 0)
            verde++;
        if (strcmp(sg.sirTriunghiuri[i]->Triunghi::getCuloare(), "albastru") == 0)
            albastru++;
        if (strcmp(sg.sirTriunghiuri[i]->Triunghi::getCuloare(), "negru") == 0)
            negru++;
        if (strcmp(sg.sirTriunghiuri[i]->Triunghi::getCuloare(), "alb") == 0)
            alb++;;
    }
    cout << "Nr figurilor de o anumita culoare:"<<endl;
    cout << "rosu: " << rosu<<endl;
    cout << "verde: " << verde<<endl;
    cout << "albastru: " << albastru<<endl;
    cout << "negru: " << negru<<endl;
    cout << "alb: " << alb<<endl;
}



Triunghi* SuprafataGrafica::findMaxAria(const SuprafataGrafica &sg)
{
    if (sg.indexTriunghiuri <= 0)
        return nullptr;
    double max = sg.sirTriunghiuri[0]->getAria();
    double aux;
    int index =0;
    for (int i=0; i<sg.indexTriunghiuri; i++)
    {
        aux = sg.sirTriunghiuri[i]->getAria();
        if (aux>max)
        {
            max=aux;
            index=i;
        }
    }
    return sg.sirTriunghiuri[index];
}
