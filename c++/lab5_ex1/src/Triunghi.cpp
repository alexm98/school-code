#include "Triunghi.h"
#include<iostream>
#include<cmath>
using namespace std;

Triunghi::Triunghi(double x1, double y1, double x2, double y2,double x3, double y3, const char *culoare)
{
    cout << "Se creaza un triunghi la adresa " << this << endl;
    p1 = new Point2D(x1,y1);
    p2 = new Point2D(x2,y2);
    p3 = new Point2D(x3,y3);
    this->culoare = culoare;
}

Triunghi::~Triunghi()
{
    if (p1!=nullptr)
        delete p1;
    if (p2!=nullptr)
        delete p2;
    if (p3!=nullptr)
        delete p3;
    cout << "Se distruge un triunghi la adresa " << this << endl;   //LA CE FOLOSESTE "THIS" ?
}

double Triunghi::getAria() const
{
    double a = distance(*p1,*p2);
    double b = distance(*p1,*p3);
    double c = distance(*p3,*p2);
    double p = (a+b+c)/2;
    return sqrt(p*(p-a)*(p-b)*(p-c));
}

 void Triunghi::afisare() const{
    cout << "Triunghi [";
    p1->afisare();
    cout << ", ";
    p2->afisare();
    cout << ", ";
    p3->afisare();
    cout << ", ";
    cout << this->culoare;
    cout<<"]"<<endl;
 }
