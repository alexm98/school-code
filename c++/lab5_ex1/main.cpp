#include <iostream>
#include "include/Linie.h"
#include "include/Point2D.h"
#include "include/Triunghi.h"
#include"include/SuprafataGrafica.h"
#include <unistd.h>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;

void punctulA()
{
    cout <<"------------------ punctul a ------------------" << endl;

    Triunghi *t1 = new Triunghi(-2,0, 0,-2, -2,-2, "rosu");
    Triunghi *t2 = new Triunghi(-2,-2, -2,2, 2,-2, "negru");
    Triunghi *t3 = new Triunghi(-1,0, 0,-1, -1,-1);
    t1->afisare();
    t2->afisare();
    t3->afisare();      //DE CE AFISEAZA "ALB" ?
}

void punctulB()
{
    cout <<"------------------ punctul b ------------------" << endl;

    SuprafataGrafica sg;
    int t1[]= {-2,0, 0,-2, -2,-2};
    sg.addFigura(TRIUNGHI, t1, "albastru");
    int t2[]= {-2,-2, -2,2, 2,-2};
    sg.addFigura(TRIUNGHI, t2, "rosu");
    int t3[]= {-1,0, 0,-1, -1,-1};
    sg.addFigura(TRIUNGHI, t3, "negru");

    sg.afisare(TRIUNGHI);
}

void punctulC()
{
    cout <<"------------------ punctul c ------------------" << endl;

    SuprafataGrafica sg;
    int t1[]= {-2,0, 0,-2, -2,-2};
    sg.addFigura(TRIUNGHI, t1, "alb");
    int t2[]= {-2,-2, -2,2, 2,-2};
    sg.addFigura(TRIUNGHI, t2, "verde");
    int t3[]= {-1,0, 0,-1, -1,-1};
    sg.addFigura(TRIUNGHI, t3, "negru");
    int t4[]= {-2,-2, -2,2, 2,-2};
    sg.addFigura(TRIUNGHI, t4, "albastru");

    Triunghi *pt = SuprafataGrafica::findMaxAria(sg);
    cout<< "Triunghiul cu aria maxima este "; pt->afisare();
}

void punctulD()
{
    cout <<"------------------ punctul d ------------------" << endl;

    SuprafataGrafica sg;
    int t1[]= {-2,0, 0,-2, -2,-2};
    sg.addFigura(TRIUNGHI, t1, "alb");
    int t2[]= {-2,-2, -2,2, 2,-2};
    sg.addFigura(TRIUNGHI, t2, "verde");
    int t3[]= {-1,0, 0,-1, -1,-1};
    sg.addFigura(TRIUNGHI, t3, "verde");
    int t4[]= {-2,-2, -2,2, 2,-2};
    sg.addFigura(TRIUNGHI, t4, "albastru");

    SuprafataGrafica::statistica(sg);
}

void punctulE()
{
    cout <<"------------------ punctul e ------------------" << endl;

    /*
    Linie *l1 = new Linie(-2,0, 0,-2, "rosu");
    Linie *l2 = new Linie(-1,0, 0,-1, "negru");
    Linie *l3 = new Linie(-4,-2, 2,2);

    l1->afisare();
    l2->afisare();
    l3->afisare();

    delete l1;
    delete l2;
    delete l3;
    */
    //STERGETI /* si */ DUPA CE IMPLEMENTATI CLASA LINIE
}

void punctulF()
{
    cout <<"------------------ punctul f ------------------" << endl;

    /*
    SuprafataGrafica sg;
    int l1[]= {-2,0, 0,-2};
    sg.addFigura(LINIE, l1, "rosu");
    int l2[]= {-1,0, 0,-1};
    sg.addFigura(LINIE, l2, "negru");

    sg.afisare(LINIE);
    */
    //STERGETI /* si */ DUPA CE IMPLEMENTATI CLASA LINIE
}

int main()
{
    punctulA();
    punctulB();
    punctulC();
    punctulD();
    punctulE();
    punctulF();

    return 0;

}
