#ifndef POINT2D_H
#define POINT2D_H
#include <unistd.h>
#include <cstdlib>
#include <ctime>
#define POZITIVE_AXE_LENGTH 400

class Point2D
{
public:
    Point2D();
    Point2D(double x, double y);
    ~Point2D();

    static int getCount();
    friend double distance(const Point2D& a, const Point2D &b);
    static bool equals(const Point2D&, const Point2D &);
    void afisare() const;

    double getX() const
    {
        return this->x;
    }

    double getY() const
    {
        return this->y;
    }

    void translate(const int, const int);
    static double getValidCoordonateValue()
    {
        long v = std::rand()%POZITIVE_AXE_LENGTH;
        return std::rand()%2==0?v:-v;
    }

private:
    double x;
    double y;
    static int count;

    bool validCoordonate(double v)
    {
        return (v>-POZITIVE_AXE_LENGTH) && (v<POZITIVE_AXE_LENGTH);
    }
};

#endif // POINT2D_H
