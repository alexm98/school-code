#include "Point2D.h"
#include "Linie.h"
#include "Triunghi.h"
#ifndef SUPRAFATAGRAFICA_H
#define SUPRAFATAGRAFICA_H

enum TIP_FIGURA {PUNCT, LINIE, TRIUNGHI};

class SuprafataGrafica
{
public:
    SuprafataGrafica();
    virtual ~SuprafataGrafica();

    void addFigura(TIP_FIGURA t, int *coordonate, const char *culoare);  //CE REPREZINTA "int *coordonate" SI DE CE O FOLOSIM ?
    void afisare(TIP_FIGURA t) const;
    static void statistica(const SuprafataGrafica&);
    static Triunghi* findMaxAria(const SuprafataGrafica &);

private:
    static const int DIM_SIR = 2;
    Point2D **sirPuncte;
    Linie **sirLinii;
    Triunghi **sirTriunghiuri;
    int indexPuncte;
    int indexLinii;
    int indexTriunghiuri;
};

#endif // SUPRAFATAGRAFICA_H
