#include "Point2D.h"
#ifndef TRIUNGHI_H
#define TRIUNGHI_H
using namespace std;

class Triunghi
{
public:
    Triunghi(double x1=0, double y1=0, double x2=0, double y2=0,double x3=0, double y3=0, const char *culoare="alb" );
    virtual ~Triunghi();
    void afisare() const;
    double getAria() const;

    const char * getCuloare() const
    {
        return culoare;
    }

private:
    Point2D *p1, *p2, *p3;
    const char *culoare;
};

#endif // TRIUNGHI_H

