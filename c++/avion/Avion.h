#ifndef AVION_H
#define AVION_H
#include <string>

using namespace std;

class Avion
{
    public:
        Avion(); // Constructor Gol
        Avion(string, int);
        virtual ~Avion();
        void Afisare();
        friend ostream operator << (ostream &out, const Avion &avion);

    protected:
        string combustibil;// -> este o variabila adica un parametru am zis variabila
        int pret;
    private:
};

#endif // AVION_H
