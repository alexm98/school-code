#include "Firma.h"
#include "Avion.h"
#include<iostream>

using namespace std;
Firma::Firma(){}
Firma::~Firma(){}

Firma::Firma(string nume, vector<Avion> c)
{
    this->nume = nume; // atribuire de constante
    this->nravioane = c.size(); // preia nr de avione cu ajutorul size() de la vecotor care returneaza cate elemente sunt in vector
    this->modele = c; // atribuire de vector

}
void Firma::Afisare(){

    Avion model; //Creez un nou obiect model
    int l = this->nravioane; //Calculez cat de un e vectorul adica cate elemente are
    cout << "Firma: " << this->nume << endl; // Afisare numele firmei
    cout << "Numar de avioane: " << this->nravioane << endl; //Afisare numar de avioane
    cout << "---- Lista Avioane ----" << endl; //Printare text
    for (int i = 0;i<l;i++) // For folosit pentru a lua fiecare element la rand din vector dupa indexul i, in vector indexul incepe de la 0
    {
        this->modele[i].Afisare();//Apelare functia afisare din clasa AVION nu din clasa FIRMA
    }
    cout << "-----------------------" << endl; // Printare text
    cout << endl; // Printare rand liber

}
ostream operator <<(ostream &, Firma &firma){
    firma.Afisare();
}
