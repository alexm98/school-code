#ifndef FIRMA_H
#define FIRMA_H
#include <iostream>
#include "Avion.h"
#include <vector>

using namespace std;

class Firma
{
    public:
        Firma();
        Firma(string, vector<Avion>);
        virtual ~Firma();
        void Afisare();
        friend ostream operator <<(ostream &, const Firma &firma);

        string nume;
        int nravioane;
        vector<Avion> modele;
    protected:
    private:
};

#endif // FIRMA_H
