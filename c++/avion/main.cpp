#include <iostream>
#include "Avion.h"
#include "Firma.h"
#include <vector>

using namespace std;

int main()
{

    vector<Avion> c;

    c.push_back(Avion ("Diesel", 2000));
    c.push_back(Avion ("Diesel", 2000));
    c.push_back(Avion ("Diesel", 2000));
    c.push_back(Avion ("Diesel", 2000));

    vector<Firma> firme;

    firme.push_back(Firma ("Ferray", c));
    firme.push_back(Firma ("Audi", c));
    firme.push_back(Firma ("BMW", c));
    firme.push_back(Firma ("Nissan", c));
    firme.push_back(Firma ("ETC", c));

    int l = firme.size();

    for(int i = 0; i < l; i++)
    {
        cout<<firme[i];
    }
}
