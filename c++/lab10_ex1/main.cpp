#include <iostream>
#include <typeinfo>
#include "AparatElectronic.h"
#include "Scanner.h"
#include "Imprimanta.h"
#include "Copiator.h"

using namespace std;

int main()
{
    cout<<".......... Punctul A .........."<<endl;
    AparatElectronic ae1(10.50);
    AparatElectronic ae2(ae1);
    cout<<ae1<<endl;
    cout<<"ae1 pret: "<<ae1.getPret()<<endl;
    ae1.setPret(11.50);
    cout<<ae1<<endl;
    cout<<ae2<<endl;

    cout<<".......... Punctul B .........."<<endl;
    Scanner sc1(20.90, 100);
    Scanner sc2(sc1);
    cout<<sc1<<endl;
    cout<<"sc1 pret: "<<sc1.getPret()<<endl;
    sc1.setPret(21.99);
    cout<<sc1<<endl;
    cout<<sc2<<endl;

    cout<<".......... Punctul C .........."<<endl;
    Imprimanta im1(50.90, true);
    Imprimanta im2(im1);
    cout<<im1<<endl;
    cout<<"im1 pret: "<<im1.getPret()<<endl;
    im1.setPret(60.95);
    cout<<im1<<endl;
    cout<<im2<<endl;

    cout<<".......... Punctul D .........."<<endl;
    Copiator cp1(100.90, 110, true, 2017);
    Copiator cp2(cp1);
    cout<<cp1<<endl;
    cout<<"cp1 pret: "<<cp1.getPret()<<endl;
    cp1.setPret(110.95);
    cout<<cp1<<endl;
    cout<<cp2<<endl;

    cout<<".......... Punctul E .........."<<endl;
    Copiator c(20,100,false,2011); cout<<endl<<c<<endl<<endl;
    c.Scanner::setPret(10); c.Imprimanta::setPret(20);
    cout << "Prec copiator (Scanner)" << c.Scanner::getPret()<<endl;
    cout << "Prec copiator (Imprimanta)" << c.Imprimanta::getPret()<<endl;

    cout<<".......... Punctul F .........."<<endl;
    AparatElectronic tablou[5];
    tablou[0] = ae1;
    tablou[1] = sc1;
    tablou[2] = im1;
    tablou[3] = cp1;
    tablou[4] = Scanner(10.60,50);
    cout<<"Sirul de obiecte:"<<endl;
    for (int i=0; i<5; i++)
        cout<<tablou[i]<<endl;

    cout<<".......... Punctul G .........."<<endl;
    AparatElectronic *tab2[3];
    tab2[0] = new AparatElectronic(10);
    tab2[1] = new Scanner(15,100);
    tab2[2] = new Scanner(30,150);

    int count = 0;
    for (int i=0; i<3; i++) {
        if (typeid(*tab2[i]) == typeid(Scanner))
            count++;
    }
    cout<<"Nr. obiectelor de tip Scanner: "<<count<<endl;

    return 0;
}
