#ifndef APARATELECTRONIC_H
#define APARATELECTRONIC_H

#include <iostream>
using namespace std;


class AparatElectronic
{
    public:
        AparatElectronic();
        AparatElectronic(float pret);
        virtual ~AparatElectronic();
        virtual void setPret(float pret);
        virtual float getPret();
        friend ostream& operator<<(ostream&, AparatElectronic);
    protected:
        float pret;
    private:
};

#endif // APARATELECTRONIC_H
