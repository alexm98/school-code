#ifndef IMPRIMANTA_H
#define IMPRIMANTA_H

#include <AparatElectronic.h>
#include <iostream>
using namespace std;


class Imprimanta : public virtual AparatElectronic
{
    public:
        Imprimanta();
        Imprimanta(float pret, bool fataVerso);
        virtual ~Imprimanta();
        friend ostream& operator<<(ostream&, Imprimanta);
    protected:
        bool fataVerso;
    private:
};

#endif // IMPRIMANTA_H
