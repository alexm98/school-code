#ifndef COPIATOR_H
#define COPIATOR_H

#include <Scanner.h>
#include <Imprimanta.h>
#include <iostream>
using namespace std;


class Copiator : public Scanner, public Imprimanta
{
    public:
        Copiator();
        Copiator(float pret, int vitezaScanare, bool fataVeso, unsigned int anFabricatie);
        virtual ~Copiator();
        friend ostream& operator<<(ostream&, Copiator);
    protected:
    private:
        unsigned int anFabricatie;
};

#endif // COPIATOR_H
