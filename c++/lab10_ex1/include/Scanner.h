#ifndef SCANNER_H
#define SCANNER_H

#include <AparatElectronic.h>
#include <iostream>
using namespace std;


class Scanner : public virtual AparatElectronic
{
    public:
        Scanner();
        Scanner(float pret, int vitezaScanare);
        virtual ~Scanner();
        friend ostream& operator<<(ostream&, Scanner);
    protected:
        int vitezaScanare;
    private:
};

#endif // SCANNER_H
