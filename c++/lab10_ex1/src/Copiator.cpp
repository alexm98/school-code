#include "Copiator.h"

Copiator::Copiator()
{
    //ctor
}

Copiator::Copiator(float pret, int vitezaScanare, bool fataVerso,
                   unsigned int anFabricatie) : AparatElectronic(pret), Scanner(pret, vitezaScanare), Imprimanta(pret, fataVerso)
{
    this->anFabricatie = anFabricatie;
}

Copiator::~Copiator()
{
    //dtor
}

ostream& operator<<(ostream& out, Copiator cp)
{
    if (cp.Imprimanta::fataVerso == true) {
        out<<"Copiator, pret: "<<cp.Scanner::pret<<" RON, viteza de scanare: "
        <<cp.Scanner::vitezaScanare<<" ppm, printeaza fata verso: DA, anul fabricatiei: "<<cp.anFabricatie;
    }
    else {
        out<<"Copiator, pret: "<<cp.Scanner::getPret()<<" RON, viteza de scanare: "
        <<cp.Scanner::vitezaScanare<<" ppm, printeaza fata verso: NU, anul fabricatiei: "<<cp.anFabricatie;
    }
    return out;
}
