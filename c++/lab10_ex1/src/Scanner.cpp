#include "Scanner.h"


Scanner::Scanner()
{
    //ctor
}

Scanner::Scanner(float pret, int vitezaScanare) : AparatElectronic(pret)
{
    this->vitezaScanare = vitezaScanare;
}

Scanner::~Scanner()
{
    //dtor
}

ostream& operator<<(ostream& out, Scanner sc)
{
	out<<"Scanner, pret: "<<sc.pret<<" RON, viteza de scanare: "<<sc.vitezaScanare<<" ppm";
	return out;
}
