#include "AparatElectronic.h"


AparatElectronic::AparatElectronic()
{
    //ctor
}

AparatElectronic::AparatElectronic(float pret)
{
    this->pret = pret;
}

AparatElectronic::~AparatElectronic()
{
    //dtor
}

void AparatElectronic::setPret(float pret)
{
    this->pret = pret;
}

float AparatElectronic::getPret()
{
    return pret;
}

ostream& operator<<(ostream& out, AparatElectronic ae)
{
	out<<"Aparat Electronic, pret: "<<ae.pret<<" RON";
	return out;
}
