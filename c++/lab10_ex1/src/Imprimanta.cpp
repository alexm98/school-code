#include "Imprimanta.h"


Imprimanta::Imprimanta()
{
    //ctor
}

Imprimanta::Imprimanta(float pret, bool fataVerso) : AparatElectronic(pret)
{
    this->fataVerso = fataVerso;
}

Imprimanta::~Imprimanta()
{
    //dtor
}

ostream& operator<<(ostream& out, Imprimanta im)
{
    if (im.fataVerso == true) {
        out<<"Imprimanta, pret: "<<im.pret<<" RON, printeaza fata verso: DA";
    }
    else {
        out<<"Imprimanta, pret: "<<im.pret<<" RON, printeaza fata verso: NU";
    }
	return out;
}
