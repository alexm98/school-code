#Problema 0 = Aria triunghiu de sub diagonala principala a patratului unitate
problema0=function(N){
n=0
for (i in 1:N){
x=runif(1)
y=runif(1)
if (y <= x) n=n+1
}
return (n/N)
}

problema0(10000)



# Problema 1:
# Folositi metoda Monte Carlo pentru a estima aria regiunii de sub curba y = x^2 din patratul unitate:


aria1=function(n) {
                   k=0
                   for (i in 1:n) {
                                   x=runif(1)
                                   y=runif(1)
                                   if (y<=x^2) k=k+1
                                  }
                   return(k/n)
                  }
aria1(100000)

f <- function(x) {x^2}
integrate(f,0,1)


# Problema 2
# Estimatia aria cercului cu centrul in punctul de coordonate (0.5; 0.5) si raza 0.5.

aria2=function(n){
                  k=0
                  for (i in 1:n) {
                                   x=runif(1)
                                   y=runif(1)
                                   if ((x-0.5)^2+(y-0.5)^2<=0.5^2) k=k+1
                                  }
                   return(k/n)
                 }
aria2(100000)
aria2(500000)/0.5^2
pi

# Problema 3
# Estimatti aria regiunii de sub curba y =1/(x+1)
g<-function(x) 1/(x+1)
plot(g,0,1)
aria3=function(n){
                  k=0
                  for (i in 1:n) {
                                   x=runif(1)
                                   y=runif(1)
                                   if (y<=1/(x+1)) k=k+1
                                  }
                   return(k/n)
                 }
aria3(100000)
integrate(g,0,1)

# Problema 4
# Acul lui Buffon

buffon=function(n){
                  k=0
                  for (i in 1:n) {
                                   theta=runif(1,0,pi/2)
                                   d=runif(1,0,1/2)
                                   if (d<=1/2*sin(theta)) k=k+1
                                  }
                   return(k/n)
                 }
buffon(100000)
#care este probabilitatea?
#se poate estima aria aria suprafetei hasurate?
buffon(100000)*pi/4
h <- function(x){sin(x)/2}
integrate(h,0,pi/2)
# se poate estima si Pi
2/buffon(100000) 
pi



