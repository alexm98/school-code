fractal <- function(nr_puncte){
    x = c(nr_puncte)
    y = c(nr_puncte)
    x[1] <- sample(0:100,1)
    y[1] <- sample(0:100,1)
    for(i in 1:nr_puncte){
        decizie <- sample(0:2,1)
        # daca ne indreptam spre P
        if(decizie == 0){
            x[i+1] <- (x[i] + 1)/2
            y[i+1] <- (y[i] + 1)/2
        }
        # daca ne indreptam spre Q
        else if(decizie == 1){
            x[i+1] <- (x[i] + 50)/2
            y[i+1] <- (y[i] + 95)/2
        }
        # altfel doar spre R
        else{
            x[i+1] <- (x[i] + 99)/2
            y[i+1] <- (y[i] + 1)/2
        }
    }
    plot(x,y)
}
