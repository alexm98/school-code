import java.util.Arrays;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Chess game1 = new Chess("Gamenr1");
		TicTacToe game2 = new TicTacToe("Tictactoe");
		RubikCube game3 = new RubikCube("rubikgame");
		
		Game table[] = new Game[3];
		table[0] = game1;
		table[1] = game2;
		table[2] = game3;
		
		TestClass tester = new TestClass();
		//tester.testGame(game1);
		//tester.testGame(game2);
		//tester.testGame(game3);
		
		tester.createTable(table);
		//Arrays.sort(table);
		//tester.createTable(table);
	}
	
}
