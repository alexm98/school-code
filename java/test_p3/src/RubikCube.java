
public class RubikCube implements Game{
	public String gamename;
	final private int players=1;
	
	public RubikCube(String gamename){
		this.gamename = gamename;
	}

	@Override
	public String toString() {
		return "Gamename= " + gamename + ", players= " + players;
	}

	public String getName() {
		return gamename;
	}

	public void setName(String gamename) {
		this.gamename = gamename;
	}

	public int getPlayers() {
		return players;
	}
}
