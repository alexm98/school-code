
public interface Game {
	public String getName();
	public int getPlayers();
}
