public class TicTacToe implements Game,Board{
	public String gamename;
	private int players;
	private int boardsize;
	
	public TicTacToe(String gamename){
		this.gamename = gamename;
		this.players = 2;
		this.boardsize = 9;
	}

	public String toString() {
		return "Gamename= " + gamename + ", players= " + players + ", boardsize= " + boardsize;
	}

	public String getName() {
		return gamename;
	}

	public void setName(String gamename) {
		this.gamename = gamename;
	}

	public int getPlayers() {
		return players;
	}

	public void setPlayers(int players) {
		this.players = players;
	}

	public int getSquare() {
		return boardsize;
	}

	public void setSquare(int boardsize) {
		this.boardsize = boardsize;
	}
	
	
}