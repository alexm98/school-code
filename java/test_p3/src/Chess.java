
public class Chess implements Game,Board{
	public String gamename;
	final private int players;
	final private int boardsize;

	public Chess(String gamename){
		this.gamename = gamename;
		this.players = 2;
		this.boardsize = 64;
	}

	public String getName() {
		return gamename;
	}

	public void setName(String gamename) {
		this.gamename = gamename;
	}

	public String toString() {
		return "Gamename= " + gamename + ", players= " + players + ", boardsize= " + boardsize;
	}

	public int getPlayers() {
		return players;
	}

	public int getSquare() {
		return boardsize;
	}
}
