import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Employee implements Serializable{
	private String name;
	private String cnp;
	private int salary;
	
	public Employee(String name,String cnp, int salary){
		this.name = name;
		this.cnp = cnp;
		this.salary = salary;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public void print(){
		System.out.println("Name: "+ this.name + ", CNP : " + this.cnp + ", Salary: " + this.salary);
	}
	
	public void writeToFile(){
		try{
			File f = new File("employees.txt");
			if(!f.exists()){
				FileOutputStream file = new FileOutputStream("employees.txt");
				ObjectOutputStream output = new ObjectOutputStream(file);
				output.writeObject(this);
				output.close();
				file.close();
			}
			else{
				FileOutputStream file = new FileOutputStream("employees.txt",true);
				ObjectOutputStream output = new ObjectOutputStream(file){
					@Override
			        protected void writeStreamHeader() throws IOException {
			            reset();
			        }
				};
				output.writeObject(this);
				output.close();
				file.close();
				
			}
		}
		catch(IOException i){
			i.printStackTrace();
		}
	}
}