import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class Company {
	private String name;
	private List<Employee> emp = new ArrayList<Employee>();
	
	public Company(String companyname){
		this.name = companyname;
		try{
			FileInputStream file = new FileInputStream("employees.txt");
			ObjectInputStream input = new ObjectInputStream(file);
			//for reading multiple employees from the object file
			try{
				while(true){
					Employee e = (Employee) input.readObject();
					emp.add(e);	
				}
			}
			catch(EOFException e){
				// leave this empty.
			}
			input.close();
			file.close();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		catch(ClassNotFoundException a){
			a.printStackTrace();
		}
	}
	
	public void getEmployees(){
		for(Employee e:emp){
			e.print();
		}
	}
}