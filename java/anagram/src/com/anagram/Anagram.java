package com.anagram;

import java.lang.*;
import java.util.Random;

public class Anagram {

	/**
	 * @param args
	 */
	private String word;
	public String result;
	
	public Anagram(String input){
		this.word = input;
	}
	
	public String getAnagram(){
		return createAnagram();
	}
	
	private String createAnagram(){
		String anyword = this.word;
		char nagaram[] = anyword.toCharArray();
		
		for(int i = 0; i < anyword.length(); i++){
			//int j = (int)Math.random()*(anyword.length()-1);
			Random rand = new Random();
			int j = rand.nextInt(anyword.length());
			
			char tmp = nagaram[i];
			nagaram[i] = nagaram[j];
			nagaram[j] = tmp;
			
		}
		//System.out.println(result);
		return new String(nagaram);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Anagram a = new Anagram("abcdefg");
		String test = a.getAnagram();
		System.out.println(test);
	}

}
