import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Uvt extends JApplet implements ActionListener{
	private JTextField textfield1;
	private JComboBox combo1;
	public Uvt() {
		
        try{
            SwingUtilities.invokeAndWait(new Runnable(){
            	public void run(){
                    createGUI();
                }
            });
        } catch (Exception e) { 
            System.err.println("Could not create GUI");
        	}
	}
		
	
        public void createGUI() {
            JPanel panel = new JPanel();
            panel.setBackground(Color.gray);  
            panel.setLayout(new GridLayout(4,2));
            
            JLabel label1 = new JLabel("UVT");
            JLabel label2 = new JLabel("Name");
            JLabel label3 = new JLabel("Salary");
            JLabel empty = new JLabel("");
            
            String names[]={"Ion","Alex","Vlad","Ionut","Mihai"};        
            combo1 = new JComboBox(names);
            combo1.addActionListener(this);
            
            textfield1 = new JTextField();
            JTextField textfield2 = new JTextField();
            
            JButton update = new JButton("Update");
            
            this.add(panel);
            panel.add(label1);
            panel.add(combo1);
            panel.add(label2);
            panel.add(textfield1);
            panel.add(label3);
            panel.add(textfield2);
            panel.add(empty);
            panel.add(update);
		} 
        public void actionPerformed(ActionEvent e) { 
			textfield1.setText((String)combo1.getSelectedItem());
        	//System.out.println("Merge");
        }
}