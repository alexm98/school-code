package com.main;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Carte a = new Carte();
		a.setAutor("Ion");
		a.setTitlu("Whatever");
		System.out.println(a.getAutor());
		System.out.println(a.getTitlu());*/
		Book b = new Book("Ion","Whatever",20);
		Book c = new Book("Test","abcd",42);
		Book d = new Book("Test2","aaaa",200);
		b.setPages(50);
		/*Attributes declared with the final modifier cannot be changed, not even without methods inside the class*/
		//b.setAutor("Vasile");
		System.out.println(b.toString());
		//System.out.println(d instanceof Book);
		System.out.println("No. of book objects: "+d.bookcount);
	}

}
