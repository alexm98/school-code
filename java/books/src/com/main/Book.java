package com.main;

public class Book{

	/**
	 * @param args
	 */
	final private String author;
	private String title;
	private int pages;
	public static int bookcount;
	
	public Book(String authorname,String titlename,int pagenr){
		this.author = authorname;
		this.title = titlename;
		this.pages = pagenr;
		bookcount++;
	}
	
	public String toString(){
		return "Author: "+this.author+", Title: "+this.title+", Page number: "+this.pages;
	}
		
	public String getTitle(){
		return this.title;
	}
	
	public void setTitle(String newtitle){
		this.title = newtitle;
	}
	
	public int getPages(){
		return this.pages;
	}
	
	public void setPages(int newpagenr){
		this.pages = newpagenr;
	}

}
