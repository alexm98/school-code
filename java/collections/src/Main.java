import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Employee> employees = new ArrayList();
		employees.add(new Employee("Ion",2300));
		employees.add(new Employee("Gheorghe",2500));
		employees.add(new Employee("Vasile",3400));
		
		/*
		for(Employee e:employees){
			System.out.println(e.toString());
		}*/
		
		List<String> jobs = new ArrayList();
		jobs.add("boss");
		jobs.add("developer");
		jobs.add("door guard");
	
		/*
		for(String s:jobs){
			System.out.println(s);
		}*/
	
		Map<Employee,String>  m = new HashMap<Employee,String>();
		
		for(int i=0;i<employees.size();i++){
			m.put(employees.get(i),jobs.get(i));
		}
		
		// iterate using an Iterator
		Iterator<Map.Entry<Employee,String>> it = m.entrySet().iterator();
		while(it.hasNext()){
			 Map.Entry<Employee,String> entry = it.next();
			 System.out.println("Key : " + entry.getKey().getName() + ", Value : " + entry.getValue());
		}
		
		/* iterate with for loop
		for(Map.Entry<Employee,String> entry: m.entrySet()){
			System.out.println(entry.getKey()+ " Position: " +entry.getValue());
		}*/
	}
}