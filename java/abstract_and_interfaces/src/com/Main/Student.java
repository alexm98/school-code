/**
 * 
 */
package com.Main;

/**
 * @author alex
 *
 */
public class Student extends Person{
	private double grade;
	
	public Student(String name,int age, double grade){
		super(name,age);
		this.grade = grade;
	}
	// @Override -> for overriding the base class's function
	@Override
	public String toString(){
		return "Name: "+this.name+", Age: "+this.age+" Grade: "+this.grade;
	}
	
	public String getName(){
		return this.name;
	}

	public void setName(String newname){
		this.name = newname;
	}

	public int getAge(){
		return this.age;
	}

	public void setAge(int newage){
		this.age = newnage;
	}
	
	public double getGrade(){
		return this.grade;
	}

	public void setGrade(double newgrade){
		this.grade = newgrade;
	}

}
