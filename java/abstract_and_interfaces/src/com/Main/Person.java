/**
 * 
 */
package com.Main;

/**
 * @author alex
 *
 */
abstract public class Person implements Print,Comparable<Person>{
	/**
	 * @param args
	 */
	protected String name;
	protected int age;
	
	public Person(String name,int age){
		this.name = name;
		this.age = age;
	}
	
	abstract public String toString(){
		// abstract functions don't have an implementation!
		// return "Name: "+this.name+", Age: "+this.age;	
	}
	
	public int compareTo(Person o){
		return age-o.age;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String newname){
		this.name = newname;
	}

	public int getAge(){
		return this.age;
	}

	public void setAge(int newage){
		this.age = newnage;
	}
}