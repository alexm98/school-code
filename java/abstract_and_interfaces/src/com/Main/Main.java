package com.Main;
import java.util.Arrays;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Person ion = new Person("Ion",24);
		Person vlad = new Person("Vlad",20);
		System.out.println(ion.toString());
		System.out.println(vlad.toString());*/
		Student s = new Student("Gheorghe",21,8.3);
		System.out.println(s.toString());
		s.setGrade(3.4);
		System.out.println(s.toString());
		Person p1 = new Student("Vasile",30,2.5);
		System.out.println(p1.toString());
		Person t[] = new Person[3];
		t[0] = new Student("Alad",24,8.5);
		t[1] = new Student("Whatever",21,4.99);
		t[2] = new Student("Test",19,5.90);
		
		System.out.println("Nesortate:");
		for(Person pers : t){
			System.out.println(pers.toString());
		}
		Arrays.sort(t);
		System.out.println("sortate:");
		for(Person pers : t){
			System.out.println(pers.toString());
		}
		System.out.println(t[2].compareTo(t[1]));
	}
}