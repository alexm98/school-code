import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Employee> employees = new ArrayList();
		
		Connection c = null;
		ResultSet results = null;
		
		final String username = new String("root");
		final String password = new String(" ");
		
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:derby://localhost/exemplu";
		try{
			c = DriverManager.getConnection(url,username,password);
	
			PreparedStatement statement = c.prepareStatement("SELECT * FROM APP.employees");
			
			results = statement.executeQuery();
			while(results.next()){
				int cnp = results.getInt("cnp");
				String name = results.getString("name");
				int salary = results.getInt("salary");
				Employee e = new Employee(name,cnp,salary);
				employees.add(e);
			}
			for(Employee e:employees){
				System.out.println(e);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	

}
