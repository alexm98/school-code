public class Employee {
	public int cnp;
	public String name;
	public int salary;

	public Employee(String name,int cnp,int salary){
		this.name = name;
		this.cnp = cnp;
		this.salary = salary;
	}

	public String toString() {
		return "Employee name = " + name + "cnp = " + cnp + ", salary = " + salary;
	}
	
}