; expresii lambda, mapare
; apply, funcall

; definire functii iterative (cu do si do*)
; definire functii in 4 moduri


; ; iterativ '(1 2 3) ---> 6
(defun f3 (l)
	(do ( (lista l (cdr lista))
	      (suma 0 (if (numberp (car lista)) 
                           (+ (car lista) suma) 
                           (+ 0 suma)
                       )
		)
	    )
	    ((endp lista) suma)
	
	)
)

; recursiva 

(defun f1 (l)
   (if (null l)   
	0                  ; cazul de baza (conditia de oprire)
	(if (numberp (car l)) 
		(+ (car l) (f1 (cdr l))) ; apel recursiv pe un obiect mai mic
		(f1 (cdr l))
	) 
   )
)

; final recursiva
; - un acumulator in care se calculeaza rezultatul pas cu pas
;  cand se ajunge la conditia de oprire rezultatul este deja obtinut in acc
; - apelul recursiv este ultimul si nu este argument pentru alta functie

(defun f2 (l)
   (f2_aux l 0) ; al doilea argument al lui f2_aux --> valoarea initiala acc
)

(defun f2_aux (l acc)
	(if (null l)
		acc    
		(f2_aux (cdr l) (+ acc (car l)))
	)
)

; folosind apply

(defun f4 (l)
	(apply '+ l)
)

;; factorial iterativ
; do sau so*

(defun fact_it (n)
	(do ( (i n (- i 1))       ; i=5  ; i=4
	      (rez 1 (* rez i))   ; rez=1 ; rez 1*5...
		)
	   ((= i 1) rez)
	)
)

(defun fact_it2 (n)
	(do* ( (i 1 (1+ i))        ; i=1    ; i=2     ; i=3    ; i=4
		(rez n (* rez i))  ; rez=5  ; rez= 5*2 ; rez= 5*2*3 ; rez=
		)
	   ((= i (- n 1)) rez)

	)

)

(defun fact_it3 (n)
	(do* ( (i 1 (1+ i))        
		(rez 1 (* rez i))  
		)
	   ((= i n) rez)

	)

)










