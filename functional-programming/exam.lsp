(defun my-reverse-acc (l acc)
    (if (null l)
        acc
        (my-reverse-acc (cdr l) (cons (car l) acc))
        )
    )

(my-reverse-acc '(1 2 3) ())


(defun my-reverse-depth (l)
    (cond ((null l) nil)
          ((atom l) l)
            (t (append (my-reverse-depth(cdr l)) (cons (my-reverse-depth(car l)) nil)))
        )
    )

(defun my-length-acc (l acc)
    (if (null l)
        acc
        (my-length-acc (cdr l) (+ 1 acc))
        )
    )

(my-length-acc '(1 2 3) 0)

(defun my-length (l)
    (if (endp l)
        0
        (+ 1 (my-length (cdr l)))
    )
)

(defun my-length-depth (l)
    (cond ((null l) 0)
          ((atom l) 1)
          (t (+ (my-length-depth (car l)) (my-length-depth (cdr l))))
        )
    )

(defun my-length-depth (l)
    (cond ((null l) 0)
        ((atom l) 1)
        (t ((mapcar my-length-depth l) (my-length-depth (cdr l))))
        )
    )

(my-length-depth '(1 2 3))

(defun make-stiva (s)

)

;; rational

(defun make-rational (a b)
    (if (zerop b)
        "B = 0"
        (cons a b)
        )
    )

(defun numarator (nr_rational)
    (car nr_rational)
)

(defun numitor (nr_rational)
    (cdr nr_rational)
)


(defun rational* (nr1 nr2)
    (make-rational (* (numarator nr1) (numarator nr2))(* (numitor nr1) (numitor nr2)))
    )

(defun invers (nr_rational)
    (make-rational (numitor nr_rational) (numarator nr_rational))
)

(defun rational/ (nr1 nr2)
    (rational* nr1 (invers nr2))
)

(defun rationalp (nr)

)
