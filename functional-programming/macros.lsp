(defmacro zece (var)
    (list 'setq var 10))

(macroexpand '(zece z))

;; backquote stopeaza evaluarea, dar virgula face resume.
(setq a 10)
`(a este ,a)

(defmacro zece(var)
    `(setq ,var 10)
)

;; @ poate fi folosit in locul virgulei

(defmacro zece(var)
    `(setq @var 10)
)

(defmacro f (l)
    `(+ ,@l)
)
