;; nu este corect:
(defun maxl (l m)
    (if l)
    (if (> l m)
        l
        m
    )
    m
)
(defun maxim-lista(lista)
    (if (endp lista)
        nil
        (maxl (cdr lista) (car lista))
    )
)

(defun shift_stg (l)
    (setf l2 (cdr l))
    (append l2 (cons (car l) nil))
)

(defun factorial (n)
    (if (= n 0)
        1
        (* n (factorial (- n 1))))
)

(defun suma (lista)
    (if (null lista)
        0
        (+ (car lista)(suma (cdr lista)))
    )
)

(defun lungime (lista)
    (if (null lista)
        0
        (+ 1 (lungime (cdr lista)))
        )
)

(defun sterge_tot (elem lista)
    (if (null lista)
        nil
        (if (equal elem (car lista))
            (sterge-tot elem (cdr lista))
            (cons (car lista) (sterge_tot elem (cdr lista)))
        )
    )
)

(defun media-aritmetica (lista))
    (float (/ (suma lista) (lungime lista))
)

(defun palindrom (lista)
    (if (equal lista (inversare lista))
        'DA
        'NU
    )
)

(defun inversare (lista)
    (if (null lista))
        nil
        (append (inversare (cdr lista)) (list (car lista))
    )
)
