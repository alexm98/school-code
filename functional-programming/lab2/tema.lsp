(defun pow(x y)
    (if (eq y 0)
        1
        (* x (putere x(- y 1)))
    )
)

(defun f2(x y)
    (* (pow x x) (pow y y))
)

(defun f3 (l)
    (if (endp l)
        0
        (if (numberp (car l))
            (+ 1 (f3 (cdr l)))
            (f3 (cdr l))
        )
    )
)

(defun aceleasi-elemente (l)
    (if (not (cadr l)) ;; Cand ramane un singur element in lista, sa nu il compare cu NIL, also daca a ajuns aici inseamna ca a parcurs tot.
        t
        (if (not (eq (car l) (cadr l)))
            nil
            (aceleasi-elemente (cdr l))
        )
    )
)
