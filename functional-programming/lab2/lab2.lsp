(defun print-list-2 (el1 el2)
    (list el1 el2))

(defun inversare (lista)
    (print-list-2 (cadr lista) (car lista)))

(defun inversare-2 (lista)
    (list (cadr lista) (car lista)))

(defun triplu (x)
    (* 3 x))

(defun suma(x y)
    (+ x y))

(defun e-numar-par (x)
    (equal (mod x 2) 0))

(defun numar-par-sau-divizibil-cu-7 (x)
    (or (e-numar-par x) (equal 0 (rem x 7))))

(defun al-treilea (lista)
    (car (cdr (cdr lista))))

(defun al-treilea-2 (lista)
    (cddar lista))

(defun len (l)
    (if l
        (+ 1 (len (cdr l)))
        0)
    )

(defun f3 (l)
    (if (endp l)
        0
        (if (numberp (car l))
            (+ 1 (f3 (cdr l)))
            (f3 (cdr l))
            )
        )
    )

(defun putere (x y)
    (if (or (< y 0)(= y 0))
        n
        (* x (putere x (- y 1)))
        )
    )
