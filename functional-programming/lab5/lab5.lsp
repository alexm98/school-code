;; lab1
;; Lungimea unei liste
;; Inversa
;; CMMDC
;; Lista cu elemente pare
;; Lista cu elemente de pe pozitii pare
;; 5 variante pentru append
;; Merge pentru 2 liste deja sortate
;; Tema: cate exercitii putem din tema 3

(defun lung(lista)
    (do ((v1 0) (+ 1 v1))
        (lista2 lista (cdr lista2))
        ((null lista2) v1))
    )

(defun inv (lista)
    (do ((lista2 lista (cdr lista2))
            (lista3 () (cons (car lista2) lista3 )))
        ((null lista2) lista3)
        )
    )

(defun elem_pare(lista)
    (do ((lista2 lista (cdr lista2))
            (lista3 ()
                (if (evenp (car lista2))
                    (append lista3 (list (car lista2)))
                    lista3
                    )
                )
            )
        ((null lista2) lista3)
        )
    )

(defun pozitiile_pare(lista)
    (do ((lista2 lista (cddr lista2))
            (lista3 () (append lista3 (list (car lista2))))
            )
        ((null lista2) lista3)
        )
    )

(defun cmmdc (a b)
    (do* ((v1 a
              (if(>= v1 v2)
                  (- v1 v2)
                  (- v2 v1)
                  )
              )
             (v2 b
                 (if(>= v2 v1)
                     (- v2 v1)
                     (- v1 v2)
                     )
                 )
             )
        ((= v1 v2) v1)
        )
    )


(defun app (lista1 lista2)
    (if (null lista1)
        lista2
        (cons (car lista1) (app (cdr lista1) lista2))
        )
    )

(defun app (lista1 lista2)
    (if (null lista1)
        lista2
        (cons (car lista1) (app (cdr lista1) lista2))
        )
    )
