#!/bin/bash

echo
echo START $0
echo Nr argumentelor este: $#
if [$# = 2]
    then
        if[$1 = $2]
            then
                echo $1 este egal cu $2
            else
                echo $1 este diferit de $2
            fi #end if
    else
        echo EROARE numar argument pe linia de comanda diferit de 2
    fi
echo
echo STOP $0
echo
exit
