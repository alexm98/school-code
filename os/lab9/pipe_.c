#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>

int main(){
    int fd[2];
    pid_t childpid;

    pipe(fd); 
    if((childpid=fork())==0){
        fprintf(stderr,"Copil :: startez comanda cat /etc/passwd \n");
        dup2(fd[1],STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        execl("/bin/cat","se ignora","/etc/passwd",NULL);
        perror("execl comanda cat esuat");
    }

    else{
        fprintf(stderr,"PARlNTE:: startez comanda sort -d\n");
        dup2(fd[0], STDIN_FILENO); 
        close(fd[0]);
        close(fd[1]);
        execl("/usr/bin/sort", "Se ignora","-d",NULL); 
    
        perror("execl comanda sort esuat");
    }
   exit(0); 
}
